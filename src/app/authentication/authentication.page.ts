import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {


  view = "main"

  slideOpts = {
    slidesPerView: 1.1,
    freeMode: true,
    loop: true,
    autoplay: true,
    centeredSlides: false
  };

  items = [
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    },
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    },
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    },
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    },
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    },
    {
      id: 1,
      img: "https://via.placeholder.com/300x200",
      name: "",
      route: "/restaurant-menu"
    }
  ]





  constructor() { }

  ngOnInit() {
  }

  segmentChanged(v) {
    this.view = v
  }

}
