import { MyVarService } from "./../../services/my-var.service";
import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class InitGuard implements CanActivate {
  constructor(private myV: MyVarService) {}
  canActivate() {
    let child = localStorage.getItem("childSelected");
    if ((child && !this.myV.haveChildren) || (!child && this.myV.haveChildren)) {
      return true;
    }
    return false;
  }
}
