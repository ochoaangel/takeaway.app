import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ShowProductPageRoutingModule } from './show-product-routing.module';
import { ShowProductPage } from './show-product.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowProductPageRoutingModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [ShowProductPage]
})
export class ShowProductPageModule { }
