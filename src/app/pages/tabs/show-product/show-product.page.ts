import { filter } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.page.html',
  styleUrls: ['./show-product.page.scss'],
})
export class ShowProductPage implements OnInit {

  heart = false
  product

  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    private router: Router,
    private apiCall: ApiCallService,
    private platform: Platform,

  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    console.log('this.myV.initShowProduct', this.myV.initShowProduct);
    console.log('this.myV.products', this.myV.products);
    this.product = this.myV.products.filter(x => x.prodId === this.myV.initShowProduct)[0];
    console.log('this.product', this.product);
  }


  btnLeerMas(idProduct) {
    console.log('idProduct', idProduct);
    this.myV.initReadMore = idProduct;
    this.router.navigateByUrl('/tabs/menu/read-more');
  }

  btnAnadirAlCarrito() {

    if (this.myV.nowCordova) {

      if (this.myV.user) {
        this.myV.initAddToCart = this.product.prodId;
        this.router.navigateByUrl('/tabs/menu/add-to-cart');

      } else {
        this.myF.showAlertOk('Notificación', 'info', 'Para agregar productos al Carrito es necesario iniciar sesión.');
        this.router.navigateByUrl('/tabs/account');
      }

    } else {
      // caso web

      console.log('this.myV.intoLocalMode', this.myV.intoLocalMode);

      if (this.myV.intoLocalMode === 'autoServicio') {
        this.myV.initAddToCart = this.product.prodId;
        this.router.navigateByUrl('/tabs/menu/add-to-cart');
      } else if (this.myV.intoLocalMode === 'llamarCamarero') {

        console.log('btn LlamarCamarero');
        this.myF.llamarCamareroAndNotificar();
      }

    }

  }



}
