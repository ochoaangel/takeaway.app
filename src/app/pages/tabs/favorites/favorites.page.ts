import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyVarService } from 'src/app/services/my-var.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  items3 = [
    {
      id: 1,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: true
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: false,
      discount: true
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: false,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: false
    }
    , {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: false
    }
    , {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: false
    }
    , {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: false
    }
  ]
  productos


  constructor(
    private modalCtrl: ModalController,
    private apiCall: ApiCallService,
    private myVar: MyVarService,
  ) { }

  ngOnInit() {
    this.productos = this.items3

    // this.apiCall.getProducts().subscribe(prod => {
    //   console.log('prod from api:', prod);
    //   if (prod.ok) {
    //     this.productos = prod.payload
    //   } else {
    //     console.log('error en getProducts');
    //   }
    // })


  }

}
