import { Router } from '@angular/router';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Component, OnInit } from '@angular/core';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  // cantidadCarrito;
  animarCarrito = false;

  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    private router: Router,
    private apiCall: ApiCallService,
    private platform: Platform,

  ) { }

  ngOnInit() {

    // proceso para actualizar carrito y animar
    this.myV.carritoDinamico.subscribe((valor) => {
      // console.log('valorrrrrrrrrrr', valor);
      // this.myV.cantidadCarrito = valor.length;
      this.myV.cantidadCarrito = valor;
      // this.myV.cantidadCarrito = '*';
      this.animarCarrito = this.animarCarrito ? false : true;

      // quitando la clase de animación para ponerla en cualquier otro momento.
      setTimeout(() => {
        this.animarCarrito = this.animarCarrito ? false : true;
      }, 200);

    });



    // en caso de existir usuario consulta la cantidad en el carrito
    if (this.myV.user) {
      this.apiCall.cartCount().subscribe(cantidad => {
        this.myV.cantidadCarrito = cantidad.data.count;

        if (this.myV.cantidadCarrito > 0) {
          setTimeout(() => {
            this.myV.carritoActualizar(this.myV.cantidadCarrito);
          }, 5000);
        }

      });

    } else {
      this.myV.carritoActualizar(0);
    }
  }


  mostrarMensaje() {
    if (this.router.url !== '/tabs/account' && !this.myV.user && this.myV.nowCordova) {
      this.myF.showAlertOk('Notificación', 'info', 'Para Acceder al Carrito debe Iniciar Sesión.');
    }
  }

  btnLlamarCamarero() {
    console.log('btn LlamarCamarero');
    this.myF.llamarCamareroAndNotificar();
  }

}
