import { Component, OnInit, ViewChild } from "@angular/core";
import { MyVarService } from "src/app/services/my-var.service";
import { Router } from "@angular/router";
import { MyFunctionsService } from "src/app/services/my-functions.service";
import { IonSlides } from "@ionic/angular";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.page.html",
  styleUrls: ["./menu.page.scss"],
})
export class MenuPage implements OnInit {
  @ViewChild("slides", { static: false }) slider: IonSlides;

  searchResult = [];
  searchText = "";
  productsByCategorySelected = [];
  productos;
  categories;
  categoryIdSelected = -1;

  memoriaCategoria = -1;

  viewEntered = false;

  optionsSlideCategoryCarta = {
    initialSlide: 1,
    slidesPerView: 2.3,
    spaceBetween: 10,
  };

  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.ionViewWillEnter();
    setTimeout(() => {
      // caso visualizaciones
      this.viewEntered = true;

      setTimeout(() => {
        let show =
          this.categoryIdSelected && this.categoryIdSelected >= 0
            ? this.myV.categories.findIndex(
                (x) => x.catId === this.categoryIdSelected
              )
            : 0;
        this.slider.slideTo(show, 1000);
      }, 100);
    }, 100);
  }

  ionViewWillEnter() {
    this.viewEntered = false;

    if (this.myV.initMenuCategoryId && this.myV.initMenuCategoryId >= 0) {
      this.myF.updateProductsAndCategories();
      this.categoryIdSelected = this.myV.initMenuCategoryId;
      this.memoriaCategoria = this.categoryIdSelected;
      this.productsByCategorySelected = this.myV.products.filter(
        (x) => this.categoryIdSelected === x.catId
      );
      this.myV.initMenuCategoryId = -1;
    } else {
      this.myF.updateProductsAndCategories();
      // verifico si se ha cambiado el valor o se ha hecho una busqueda
      console.log(this.myV.categories);

      this.categoryIdSelected =
        this.categoryIdSelected && this.categoryIdSelected >= 0
          ? this.categoryIdSelected
          : this.myV.categories[0]["catId"];

      this.memoriaCategoria = this.categoryIdSelected;
      this.productsByCategorySelected = this.myV.products.filter(
        (x) => this.categoryIdSelected === x.catId
      );
      this.categories = this.myV.categories.map(
        (x) => this.myV.urlBaseImg + x.catIcon
      );
      console.log(this.myV.products);
    }
  }

  updateSearch(textoAbuscar: string) {
    if (textoAbuscar) {
      this.categoryIdSelected = -1;
    } else {
      this.categoryIdSelected = this.memoriaCategoria;
    }

    this.searchResult = this.myF.searchingText(textoAbuscar);
    console.log("searchResult", this.searchResult);
  }

  mostrarProducto(id) {
    this.myV.initShowProduct = id;
    this.router.navigateByUrl("/tabs/menu/show-product");
  }

  categoryPressed(idCategoryPressed) {
    console.log(idCategoryPressed);

    this.searchText = "";
    this.memoriaCategoria = idCategoryPressed;
    this.categoryIdSelected = idCategoryPressed;
    this.productsByCategorySelected = this.myV.products.filter(
      (x) => this.categoryIdSelected === x.catId
    );
  }

  updateSliderSize() {
    this.optionsSlideCategoryCarta.initialSlide = 1;
    this.optionsSlideCategoryCarta.slidesPerView = 2.3;
    this.optionsSlideCategoryCarta.spaceBetween = 30;
  }
} // fin clase ppal
