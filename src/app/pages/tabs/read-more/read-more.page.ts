import { Component, OnInit } from '@angular/core';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-read-more',
  templateUrl: './read-more.page.html',
  styleUrls: ['./read-more.page.scss'],
})
export class ReadMorePage implements OnInit {

  id;
  product;
  memoriaInitReadMore;
  optionsSlideFotos = { slidesPerView: 1.1, freeMode: false, };

  constructor(
    public myV: MyVarService,
    public apiCall: ApiCallService,
    public myF: MyFunctionsService,
    public router: Router,
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.id = this.myV.initReadMore;
    this.apiCall.productById(this.id).subscribe(resp => {
      this.product = this.prepareApi(resp);
      this.memoriaInitReadMore = this.product.prodId;
      console.log('this.product', this.product);
    });
  }

  ionViewWillLeave() { this.myV.initReadMore = ''; }

  prepareApi(fromApi) {
    let end = {
      prodId: fromApi.data.id,
      prodNombre: fromApi.data.nombre,
      prodPrecio: fromApi.data.precio,
      prodImagen: fromApi.data.imagen,
      prodDescuento: fromApi.data.descuento,
      prodImagen_min: fromApi.data.imagen_min,
      prodImagen_sqr: fromApi.data.imagen_sqr,
      prodArrGaleria: fromApi.data.json.galeria,
      prodArrAlergenos: fromApi.data.json.alergenos,
      prodDescription: fromApi.data.descripcion || null,
      prodArrIngredientes: fromApi.data.json.ingredientes,
      prodArrIngredientesNoExcluibles: fromApi.data.json.ingredientes_noexcluibles,
      prodArrIngredientesTodos: [...fromApi.data.json.ingredientes_noexcluibles, ...fromApi.data.json.ingredientes],
      prodAlergenosResumen:
        fromApi.data.json.alergenos && fromApi.data.json.alergenos.length > 0 ? this.prepareAlergenos(fromApi.data.json.alergenos) : null,
    };
    return end;
  }

  prepareAlergenos(arrayAlergenos) {
    let end = '';
    arrayAlergenos.forEach(alergeno => { end = end + alergeno + ', '; });
    end = end.slice(0, end.length - 2) + '.';
    return end;
  }

  volver() {
    this.myV.initShowProduct = this.id;
    this.router.navigateByUrl('/tabs/menu/show-product');
  }

}
