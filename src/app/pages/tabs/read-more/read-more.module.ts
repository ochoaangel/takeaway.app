import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReadMorePageRoutingModule } from './read-more-routing.module';

import { ReadMorePage } from './read-more.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReadMorePageRoutingModule,
    ComponentsModule
  ],
  declarations: [ReadMorePage]
})
export class ReadMorePageModule { }
