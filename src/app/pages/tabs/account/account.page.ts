import { Component, OnInit } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';

import { MyStorageService } from 'src/app/services/my-storage.service';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import * as _ from 'underscore';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
// var inAppBrowserRef;

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  usuario = { nombre: '', email: '', tel: '', password: '' };
  emailForgetPassword = '';

  logged = false;    // true es para usuario logeado
  showSingIn = true;
  showLogged = false;
  showSingUp = false;
  showInitType = false;
  showHistorialAll = false;
  showForgetPassword = false;
  showHistorialDetail = false;
  showSingInAnonymous = false;

  initTypeSelected: string = null;   //     null /SingIn / SingUp / SingInAnonymous

  historialDetail;

  historial;

  aceptadoTerminosRegistro = false;

  loading = 0;



  inAppBrowserRef;
  options: InAppBrowserOptions = {
    location: 'no',
    clearcache: 'yes',
    clearsessioncache: 'yes',
    beforeload: 'yes',
  };

  constructor(
    private apiCall: ApiCallService,
    public myV: MyVarService,
    public myS: MyStorageService,
    public myF: MyFunctionsService,
    private router: Router,
    private iab: InAppBrowser
  ) { }


  ngOnInit() {
    this.ocultarTodo();
  }

  ionViewWillEnter() {
    this.ocultarTodo();

    if (this.myV.user) {
      this.logged = true;
      this.showLogged = true;
      this.verificarHistorial();
    } else {
      this.showInitType = true;
    }

  }

  ///////////////////////////////////////////////////////////////////////////////
  //////////////////////////////// FUNCIONES  /////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////

  validate() {
    const end = false;

    if (this.showSingIn) {
      // Validar correo y clave

      if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.usuario.email)) {
        // alert('Ingrese un correo válido e intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese un correo válido e intente nuevamente..');

        return false;
      } else if (this.usuario.password.length < 5) {
        // alert('Ingrese una clave válida de mínimo 5 caracteres, intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese una clave válida de mínimo 5 caracteres, intente nuevamente..');

        return false;
      } else {
        return true;
      }

    } else {

      if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.usuario.email)) {
        // alert('Ingrese un correo válido e intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese un correo válido e intente nuevamente..');

        return false;
      } else if (this.usuario.password.length < 5) {
        // alert('Ingrese una clave válida de mínimo 5 caracteres, intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese una clave válida de mínimo 5 caracteres, intente nuevamente..');

        return false;
      } else if (this.usuario.nombre.length < 2) {
        // alert('Ingrese un Nombre válido, intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese un Nombre válido, intente nuevamente..');

        return false;
      } else if (this.usuario.tel.length < 6) {
        // alert('Ingrese un Teléfono válido, mínimo 6 dígitos, intente nuevamente..');
        this.myF.showAlertOk('Notificación', 'info', 'Ingrese un Teléfono válido, mínimo 6 dígitos, intente nuevamente..');

        return false;
      } else {
        return true;
      }
      // Validar nombre Tlf, correo y clave

    }
    return end;
  }


  verificarHistorial() {
    this.loading++;
    this.apiCall.clientOrders().subscribe(orders => {
      this.loading--;
      // console.log('orders', orders);
      if (orders.ok) {

        // almaceno el historial
        this.historial = orders.data;

        // organizando el historial en orden de fechas
        if (this.historial.length > 0) {
          this.historial = _.sortBy(this.historial, 'updated_at').reverse();

        }


        // preparo el texto de resumen
        this.historial.forEach(element => {
          let resumen = '';
          element.items.forEach(item => {
            resumen = resumen + `${item.cantidad || ''} ${item.item_name}, `;
          });

          // almaceno en el pedido el resumen sin la ultima coma y con punto al final
          element.resumen = resumen.substring(0, resumen.length - 2) + '.';
        });




        console.log('historial', this.historial);

      } else {
        this.historial = null;
        console.log('ERROR: Error al solicitar Historial de PEDIDOS al servidor');
      }
    });
  }

  ocultarTodo() {
    this.showLogged = false;
    this.showInitType = false;
    this.showSingIn = false;
    this.showSingUp = false;
    this.showHistorialAll = false;
    this.showHistorialDetail = false;
    this.showSingInAnonymous = false;
    this.showForgetPassword = false;
  }


  ///////////////////////////////////////////////////////////////////////////////
  //////////////////////////////// BOTONES  /////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////

  /////////////////////////// Tipos de logueo //////////////////////////////////

  btnTypeSingIn() {
    this.ocultarTodo();
    this.showSingIn = true;
    this.usuario = { nombre: '', email: '', tel: '', password: '' };
  }

  btnShowInitType() {
    this.ocultarTodo();
    this.showInitType = true;
  }

  btnTypeSingUp() {
    this.ocultarTodo();
    this.showSingUp = true;
    this.usuario = { nombre: '', email: '', tel: '', password: '' };
  }

  btnTypeSingInAnonimous() {
    this.ocultarTodo();
    this.showSingInAnonymous = true;
  }



  ////////////////////////////////// LOGUEO //////////////////////////////////

  btnSingIn() {

    if (this.validate()) {
      console.log('Si validado');

      const myData = {
        email: this.usuario.email,
        password: this.usuario.password
      };

      this.loading++;
      this.apiCall.clienLogin(myData).subscribe(resp => {
        this.loading--;

        console.log('******** Respuesta del api clientLogin');

        if (resp.ok) {

          // almaceno en localStorage
          this.loading++;
          this.myS.set('user', resp.data).subscribe(x => {
            this.loading--;

            //console.log('******** Respuesta del api set on Local Storage',JSON.stringify(x));



            // Guardo el usuario en local General
            this.myV.user = resp.data;

            this.verificarHistorial();

            // preparo todo
            this.ocultarTodo();
            this.logged = true;
            this.showLogged = true;

            this.actualizarCantidadCarrito();

          });

        } else {

          // this.btnSingOut();
          this.myF.showAlertOk('Notificación', 'info', resp.data.message);
        }
      });
    } else {
      console.log('Usuario no ingrsó los datos correctamente para iniciar Sesión..');
    }
  }

  btnShowSingIn() {
    this.showSingUp = false;
    this.showSingIn = true;
    this.usuario = { nombre: '', email: '', tel: '', password: '' };
  }


  btnSingInAnonymous() {

    if (this.validate()) {
      console.log('Si validado');

      const myData = {
        email: this.usuario.email,
        password: this.usuario.password
      };

      this.loading++;
      this.apiCall.clienLogin(myData).subscribe(resp => {
        this.loading--;

        if (resp.ok) {

          // almaceno en localStorage
          this.loading++;
          this.myS.set('user', resp.data).subscribe(x => {
            this.loading--;

            // Guardo el usuario en local General
            this.myV.user = resp.data;

            // preparo todo
            this.ocultarTodo();
            this.logged = true;
            this.showLogged = true;

            this.actualizarCantidadCarrito();

          });

        } else {

          // this.btnSingOut();
          this.myF.showAlertOk('Notificación', 'info', resp.data.message);
        }
      });
    } else {
      console.log('Usuario no ingrsó los datos correctamente para iniciar Sesión..');
    }
  }

  ////////////////////////////////// Registro //////////////////////////////////

  btnShowSingUp() {
    this.showSingUp = true;
    this.usuario = { nombre: '', email: '', tel: '', password: '' }
    this.showSingIn = false;
  }

  btnSingUp() {

    if (this.validate()) {

      console.log('Si validado');

      const myData = {
        email: this.usuario.email,
        password: this.usuario.password,
        nombre: this.usuario.nombre,
        tel: this.usuario.tel,

      };

      this.loading++;
      this.apiCall.clienRegister(myData).subscribe(resp => {
        this.loading--;

        if (resp.ok) {

          // almaceno en localStorage
          this.loading++;
          this.myS.set('user', resp.data).subscribe(x => {
            this.loading--;

            this.usuario = { nombre: '', email: '', tel: '', password: '' };

            // Guardo el usuario en local General
            this.myV.user = resp.data;

            this.verificarHistorial();

            // preparo todo para mostrar que está logueado
            this.ocultarTodo();
            this.logged = true;
            this.showLogged = true;

            this.actualizarCantidadCarrito();

            this.myF.showAlertOk('Éxito', 'done', 'Usuario registrado Exitosamente.');
          });


        } else {

          this.myF.showAlertOk('Notificación', 'info', resp.data.message);
          // this.btnSingOut();
          this.usuario = { nombre: '', email: '', tel: '', password: '' };
          this.loading++;
          this.myS.remove('user').subscribe(x => {
            this.loading--;

            // Borro el local General
            this.myV.user = null;

            this.myV.carritoActualizar(0);


            this.ocultarTodo();
            this.showInitType = true;
          });
        }
      });

    } else {
      console.log('Usuario no ingrsó los datos correctamente en el Registro..');
    }

  }

  ////////////////////////////////// Cerrar sesión //////////////////////////////////

  btnSingOut() {

    // Elimino del localStorage
    this.loading++;
    this.myS.remove('user').subscribe(x => {
      this.loading--;

      // Borro el local General
      this.myV.user = null;

      this.myV.carritoActualizar(0);


      this.myF.showAlertOk('Éxito!', 'done', 'Cerrada Sesión Exitosamente.');
      this.ocultarTodo();
      this.showInitType = true;
    });

    this.ionViewWillEnter();

  }

  ////////////////////////////////// Pedidos //////////////////////////////////

  btnMisPedidos() {
    this.ocultarTodo();
    this.showHistorialAll = true;
  }

  btnAtrasEnMisPedidosAll() {
    this.ocultarTodo();
    this.ionViewWillEnter();

  }

  btnAtrasEnMisPedidosDetalles() {
    this.ocultarTodo();
    this.showHistorialAll = true;
  }

  btnPedidoDetalle(itemPedido) {
    this.historialDetail = itemPedido;
    const products = this.myV.products;

    // buscando de cada producto comprado la imagen el productos Generales por el id
    this.historialDetail.items.forEach(unidad => {
      const idProduct = unidad.dvproduct_id || null;
      if (idProduct) {
        unidad.imagen = products.filter(x => x.prodId === idProduct)[0]['prodImagen_sqr'] || null;
      } else {
        unidad.imagen = null;
      }
    });

    // Habilitando para visualizar detalles de la compra
    this.ocultarTodo();
    this.showHistorialDetail = true;
    console.log('this.historialDetail', this.historialDetail);
  }

  actualizarCantidadCarrito() {
    this.loading++;
    this.apiCall.cartCount().subscribe(cantidad => {
      this.loading--;
      // console.log('cantidad', cantidad.data.count);
      this.myV.cantidadCarrito = cantidad.data.count;
      // this.myV.carritoActualizar(cantidad.data.count);
      this.myV.carritoActualizar(this.myV.cantidadCarrito);

    });
  }

  btnReestablecerContrasena() {

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.emailForgetPassword)) {
      this.myF.showAlertOk('Notificación!', 'info', 'Introduzca un correo válido para continuar..');

    } else {

      let myData = { email: this.emailForgetPassword }
      this.apiCall.forgotPassword(myData).subscribe(x => {
        console.log('x', x);

        if (x.ok) {
          this.myF.showAlertOk('Éxito', 'done', x.data.message);
          this.ocultarTodo();
          this.showInitType = true;

        } else {
          this.myF.showAlertOk('Notificación', 'info', x.data.message);

        }


      })

    }

  }

  btnMostrarReestablecerContrasena() {
    this.ocultarTodo();
    this.showForgetPassword = true;
  }

  btnVolverDesdeReestablecerContrasena() {
    this.ocultarTodo();
    this.showInitType = true;
  }


  btnPoliticasPrivacidad() {
    if (this.myV.appInfo.privacy_policy_url) {
      this.inAppBrowserRef = this.iab.create(this.myV.appInfo.privacy_policy_url, '_self');
    }
  }

}
