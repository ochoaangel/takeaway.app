import localeFr from '@angular/common/locales/fr';
import { Router } from '@angular/router';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Component, OnInit } from '@angular/core';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { registerLocaleData } from '@angular/common';
import { Platform } from '@ionic/angular';
registerLocaleData(localeFr, 'fr');

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.page.html',
  styleUrls: ['./add-to-cart.page.scss'],
})
export class AddToCartPage implements OnInit {
  loading = 0;
  totalGeneral = 0;
  cantidad = 1;
  precio = {
    unidad: 0,
    totalSimple: 0,
    totalMultiple: 0,
  };

  //////////////////////////////////////////////////////////////////////////////
  // obtenido desde api
  directoDesdeApi = {
    data: {
      ok: true,
      precio: 9,
      id: 1,
      nombre: '100x100',

      imagen: '/storage/5794390900075/products/100x100.png',
      imagen_sqr: '/storage/5794390900075/products/100x100-sqr.png',
      imagen_min: '/storage/5794390900075/products/thumbs/100x100.png',
      descuentos: null,
      json: {
        ingredientes: ['Queso de Cabra', 'Ketchup de Frutos Rojos', 'Cebolla Morada', 'Espinacas', 'Bacon Crujiente',],
        variaciones: [
          {
            id: 'V1',
            nombre: 'Tamaño (Ejemplo Var. Simple)',
            tipo: 'SIMPLE',
            opciones: [
              { nombre: 'Normal', valor: 0, },
              { nombre: 'Pequeña', valor: 5, },
              { nombre: 'Grande', valor: 10, },
              { nombre: 'XXL', valor: 15, },
            ],
          },
          {
            id: 'V2',
            nombre: 'Salsas (Ejemplo Var. Multiple)',
            tipo: 'MULTIPLE',
            opciones: [
              { nombre: 'Alioli', valor: 1, },
              { nombre: 'BBQ', valor: 1, },
              { nombre: 'Tzatziki', valor: 2, },
              { nombre: 'Ranchera', valor: 2, },
            ],
          },
          {
            id: 'V3',
            nombre: 'Salsas (Ejemplo Var. Multiple)',
            tipo: 'MULTIPLE',
            opciones: [
              { nombre: 'Alioli', valor: 1, },
              { nombre: 'BBQ', valor: 1, },
              { nombre: 'Tzatziki', valor: 2, },
              { nombre: 'Ranchera', valor: 2, },
            ],
          },
        ],
        alergenos: ['Gluten', 'Lactosa'],
        galeria: [
          { caption: 'Charlie Burger', image: '/storage/5794390900075/products/thumbs/100x100.png', },
          { caption: 'Charlie Burger', image: '/storage/5794390900075/products/thumbs/100x100.png', },
          { caption: 'Charlie Burger', image: '/storage/5794390900075/products/thumbs/100x100.png', },
          { caption: 'Charlie Burger', image: '/storage/5794390900075/products/thumbs/100x100.png', },
        ],
      },
    },
  };

  //////////////////////////////////////////////////////////////////////////////
  // ejemplo de como debería quedar saliendo de la funcián transformandoValoresDeApi
  productInfo = {
    precioUnidad: 10,
    id: 4552214,
    title: 'Título Producto',
    bloques: [
      {
        simple: true,
        precio: false,
        default: 1,
        nombre: 'SimpleSinPrecio',
        opciones: [
          'opcionSimple0',
          'opcionSimple1',
          'opcionSimple2',
          'opcionSimple3',
          'opcionSimple4',
          'opcionSimple5',
        ],
      },
      {
        simple: false,
        precio: false,
        default: [0, 3, 4],
        nombre: 'MultipleSinPrecio',
        opciones: [
          'opcionMultiple0',
          'opcionMultiple1',
          'opcionMultiple2',
          'opcionMultiple3',
          'opcionMultiple4',
        ],
      },
      {
        simple: true,
        precio: true,
        default: 1,
        nombre: 'SimpleConPrecio',
        opciones: [
          { nombre: 'opcionSimple0', valor: 1 },
          { nombre: 'opcionSimple1', valor: 3 },
          { nombre: 'opcionSimple2', valor: 0 },
          { nombre: 'opcionSimple3', valor: 4 },
        ],
      },
      {
        simple: false,
        precio: true,
        default: [1, 4],
        nombre: 'MultipleConPrecio',
        opciones: [
          { nombre: 'opcionMultiple0', valor: 1 },
          { nombre: 'opcionMultiple1', valor: 3 },
          { nombre: 'opcionMultiple2', valor: 4 },
          { nombre: 'opcionMultiple3', valor: 0 },
          { nombre: 'opcionMultiple4', valor: 7 },
          { nombre: 'opcionMultiple5', valor: 3 },
        ],
      },
    ],
  };

  //////////////////////////////////////////////////////////////////////////////
  // variable importante que tendrá todooo
  myData;

  directoApi;

  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    private router: Router,
    private apiCall: ApiCallService,
    private platform: Platform

  ) { }

  ngOnInit() { }

  ionViewWillEnter() {

    if (this.myV.user || this.myV.intoLocalMode === 'autoServicio') {

      // inicio variable locales generales
      this.inicializarVariablesLocales();

      this.apiCall.productById(this.myV.initAddToCart).subscribe(resp => {

        this.directoApi = resp.data;
        console.log('directoApi', this.directoApi);
        console.log('myV.urlBaseImg + directoApi.Imagen_sqr', this.myV.urlBaseImg + this.directoApi.Imagen_sqr);

        // ajustando valores de la api
        const ajutadoValoresDeApi = this.transformandoValoresDeApi(resp);

        // inicializar valores
        this.myData = this.inicializarVariablesDesdeApi(ajutadoValoresDeApi);
        console.log('myData', this.myData);

      });
    } else {
      this.router.navigateByUrl('/tabs/account');
    }


  }

  ////////////////////////////////////////////////////////////////////////
  // funciones clicks
  btnMas() {
    if (this.cantidad > 999) {
      this.cantidad = 0;
    } else {
      this.cantidad++;
    }
    this.actualizar();
  }

  btnMenos() {
    if (this.cantidad <= 1) {
      this.cantidad = 1;
    } else {
      this.cantidad--;
    }
    this.actualizar();
  }

  btnAnadirCarrito() {
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    // procesar el pedido

    this.myData.totalUnidad = this.totalGeneral;
    this.myData.cantidad = this.cantidad;

    // preparando resumen
    let bloques = this.myData.bloques;
    let resumen = '';

    // separando
    let simples = bloques.filter((key) => key.simple);
    let multiples = bloques.filter((key) => !key.simple);

    // ordenando los por precio
    simples.sort((a, b) => a.precio - b.precio).reverse();
    multiples.sort((a, b) => a.precio - b.precio).reverse();
    simples.forEach((simple) => {
      resumen = resumen + simple.selectedName + ', ';
    });

    multiples.forEach((multiple) => {
      multiple.selectedName.forEach((nombre) => {
        resumen = resumen + nombre + ', ';
      });
    });


    // borrar ultima coma de resumen
    resumen = resumen.slice(0, resumen.length - 2) + '.';

    this.myData.resumen = resumen;

    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    // procesando la info del pedido
    let pedidoPreparado = this.prepareToSendCart(this.myData);

    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    // enviando datos del carrito al server
    this.loading++;
    this.apiCall.cartAdd(pedidoPreparado).subscribe(resultado => {
      this.loading--;
      console.log('resultadoDe CartAdd', resultado);

      if (resultado.ok) {
        // caso de agregado al carrito
        console.log('Caso de Éxito al agregar al carrito..');
        // redireccionar a la página del carrito

        if (resultado.data.dvcart_uid) {
          this.myV.dvcartUID = resultado.data.dvcart_uid;
          console.log('>>>>>>>this.myV.dvcartUID ', this.myV.dvcartUID);
        }

        // mandar obj para agregar en un array
        this.actualizarCantidadCarrito();
        this.inicializarVariablesLocales();
        this.router.navigateByUrl('/tabs/menu');

      } else {
        // caso error al agregar el carrito
        console.warn('ERROR, error mandado por el servidor al haber error al agregar al carrito el producto.');
      } // FIN caso error al agregar el carrito 


    });


    console.log('pedidoPreparado', pedidoPreparado);


  }

  actualizarCantidadCarrito() {

    this.apiCall.cartCount().subscribe(cantidad => {
      // console.log('cantidad', cantidad.data.count);
      this.myV.cantidadCarrito = cantidad.data.count;
      // this.myV.carritoActualizar(cantidad.data.count);
      this.myV.carritoActualizar(this.myV.cantidadCarrito);

    });
  }
  ////////////////////////////////////////////////////////////////////////
  // funciones en el inicio
  inicializarVariablesLocales() {
    //  variables locales
    this.totalGeneral = 0;
    this.cantidad = 1;
    this.precio.unidad = 0;
    this.precio.totalSimple = 0;
    this.precio.totalMultiple = 0;
    this.myData = {};
  }

  transformandoValoresDeApi(datosAtransformar) {
    let dat = datosAtransformar.data;
    ///////////////////////////////////////////////////////////////
    // creando el obj base raiz
    let dt = {
      precioUnidad: dat.precio,
      id: dat.id,
      title: dat.nombre,
      bloques: [],
    };
    ///////////////////////////////////////////////////////////////
    // creando ingredientes
    if (dat.json.ingredientes && dat.json.ingredientes.length > 0) {
      let ingredientes = {
        simple: false,
        precio: false,
        default: [],
        nombre: 'ingredientes',
        opciones: dat.json.ingredientes
      };
      // agregando ingredientes
      dt.bloques.push(ingredientes);
    }
    ///////////////////////////////////////////////////////////////
    // iniciando variaciones (simples y multiples)
    let variaciones = [];
    dat.json.variaciones.forEach(element => {
      let item = {};

      item['simple'] = element.tipo === 'SIMPLE' ? true : false;
      item['default'] = element.tipo === 'SIMPLE' ? [0] : [];
      item['precio'] = element.opciones[0].valor || element.opciones[0].valor === 0 ? true : false;
      item['opciones'] = element.opciones;
      item['nombre'] = element.nombre;
      item['id'] = element.id;
      variaciones.push(item);
    });
    dt.bloques = [...dt.bloques, ...variaciones];

    dt['alergenos'] = dat.json.alergenos ? dat.json.alergenos : '';
    return dt;
  }

  inicializarVariablesDesdeApi(apiProductInfo): any {
    let resultado = apiProductInfo;
    this.totalGeneral = resultado.precioUnidad;
    resultado.bloques.forEach((bloque) => {
      if (bloque.precio) {
        // caso Simple
        if (bloque.simple) {
          bloque.selected = bloque.default[0] || 0;
          bloque.selectedName = bloque.opciones[bloque.default]['nombre'] || '';
          bloque.total = bloque.opciones[bloque.default]['valor'] || 0;
        } else {
          // caso Multiple

          bloque.opciones.forEach((opcion) => {
            opcion.selected = false;
          });
          bloque.selected = bloque.default;
          bloque.selectedName = [];

          bloque.total = 0;
          // tslint:disable-next-line: prefer-for-of
          for (let index = 0; index < bloque.default.length; index++) {
            const element = bloque.default[index];
            bloque.opciones[element].selected = true;
            bloque.selectedName.push(bloque.opciones[element].nombre);
            bloque.total = bloque.total + bloque.opciones[element].valor;
          }
        }
      } else {
        // caso que NO tengan precios

        // caso Simple
        if (bloque.simple) {
          bloque.selected = bloque.default || '';
          bloque.selectedName = bloque.opciones[bloque.default] || '';
        } else {
          // caso Multiple

          let result = [];
          // cambio opciones por objeto con selected false
          bloque.opciones.forEach((opcion) => {
            result.push({ nombre: opcion, selected: false });
          });
          bloque.opciones = result;
          bloque.selected = bloque.default;

          bloque.selectedName = [];
          // coloco true los que diga default
          // tslint:disable-next-line: prefer-for-of
          for (let index = 0; index < bloque.default.length; index++) {
            const element = bloque.default[index];
            bloque.opciones[element].selected = true;
            bloque.selectedName.push(bloque.opciones[element].nombre);
          }
        }
        bloque.total = 0;
      }

      // totalizando el inicio
      this.totalGeneral = this.totalGeneral + bloque.total;
      // seteando Valores iniciales
      resultado.totalUnidad = this.totalGeneral;
      // resultado.cantidadUnidad = 1;
    });

    // Gestionando los alergenos
    let resumenAlergenos = '';
    if (resultado.alergenos && resultado.alergenos.length > 0) {
      resultado.alergenos.forEach(alergeno => {
        resumenAlergenos = resumenAlergenos + `${alergeno}, `;
      });
      // borrar ultima coma de resumenAlergenos
      resumenAlergenos = resumenAlergenos.slice(0, resumenAlergenos.length - 2) + '.';
    }
    resultado.alergenosResumen = resumenAlergenos;

    return resultado;
  }

  ////////////////////////////////////////////////////////////////////////
  // funciones útilies generales
  agregarSelectedFalse(coleccion) {
    const resultado = [];
    coleccion.forEach((element) => {
      element.selected = false;
      resultado.push(element);
    });
    return resultado;
  }

  actualizar(tipo?, nbloque?, nopcion?, nombreOpcion?) {

    if (tipo) {

      if (tipo === 'simple') {
        this.myData.bloques[nbloque].selected = nopcion;
        this.myData.bloques[nbloque].selectedName = nombreOpcion;
        this.myData.bloques[nbloque].total =
          this.myData.bloques[nbloque].opciones[nopcion].valor || 0;
      } else {
        // caso Multiple

        if (this.myData.bloques[nbloque]['precio']) {
          let preSelected = [];
          let preSelectedName = [];
          let preTotal = 0;

          for (
            let index = 0;
            index < this.myData.bloques[nbloque].opciones.length;
            index++
          ) {
            const element = this.myData.bloques[nbloque].opciones[index];

            if (element.selected) {
              preSelected.push(index);
              preSelectedName.push(element.nombre);
              preTotal = preTotal + element.valor;
            }
          }

          this.myData.bloques[nbloque].selected = preSelected;
          this.myData.bloques[nbloque].selectedName = preSelectedName;
          this.myData.bloques[nbloque].total = preTotal;
        } else {
          // caso sin precio
          let preSelected = [];
          let preSelectedName = [];
          for (
            let index = 0;
            index < this.myData.bloques[nbloque].opciones.length;
            index++
          ) {
            const element = this.myData.bloques[nbloque].opciones[index];
            if (element.selected) {
              preSelected.push(index);
              preSelectedName.push(element.nombre);
            }
          }
          this.myData.bloques[nbloque].selected = preSelected;
          this.myData.bloques[nbloque].selectedName = preSelectedName;
          this.myData.bloques[nbloque].total = 0;
        }
      }
    }

    // sacando cuenta
    this.totalGeneral = this.myData.precioUnidad;
    this.myData.bloques.forEach((bloque) => {
      this.totalGeneral = this.totalGeneral + bloque.total;
    });
    this.totalGeneral = this.totalGeneral * this.cantidad;
  }

  prepareToSendCart(detallesDePedido) {
    console.log('detallesDePedido', detallesDePedido);
    let det = detallesDePedido;
    let end = {
      dvproduct_id: det.id,
      cantidad: det.cantidad,
    };

    // agrego ingredientes
    if (det.bloques.length > 0 && det.bloques[0].selected.length > 0 && det.bloques[0].nombre.toLowerCase().includes('ngredientes')) {
      end['ingredientes_sin'] = det.bloques[0].selectedName;
    }

    // preparo las variaiones
    let variaciones = det.bloques
      .map(x => {
        if (x.nombre === 'ingredientes') {
          return null;
        } else if (x.selected.length === 0) {
          return null;
        } else {
          return { id: x.id, value: x.selected };
        }
      })
      .filter(Boolean);

    end['variaciones'] = variaciones;

    return end;
  }


}
