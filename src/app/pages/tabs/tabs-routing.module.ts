import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    // redirectTo: "/tabs/menu",
    // redirectTo: '/tabs/menu/read-more',
    // redirectTo: '/tabs/cart',
    // redirectTo: '/tabs/account',
    redirectTo: '/tabs/home', //original
    pathMatch: 'full',
  },
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./home/home.module').then((m) => m.HomePageModule),
          },
        ],
      },
      {
        path: 'favorites',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./favorites/favorites.module').then(
                (m) => m.FavoritesPageModule
              ),
          },
        ],
      },
      {
        path: 'cart',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./cart/cart.module').then((m) => m.CartPageModule),
          },
        ],
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./account/account.module').then(
                (m) => m.AccountPageModule
              ),
          },
        ],
      },
      {
        path: 'menu',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./menu/menu.module').then((m) => m.MenuPageModule),
          },
          {
            path: 'show-product',
            loadChildren: () =>
              import('./show-product/show-product.module').then(
                (m) => m.ShowProductPageModule
              ),
          },
          {
            path: 'read-more',
            loadChildren: () =>
              import('./read-more/read-more.module').then(
                (m) => m.ReadMorePageModule
              ),
          },
          {
            path: 'add-to-cart',
            loadChildren: () =>
              import('./add-to-cart/add-to-cart.module').then(
                (m) => m.AddToCartPageModule
              ),
          },
        ],
      },
    ],
  },

  // { path: 'read-more'   , loadChildren: () => import('./read-more/read-more.module'      ).then( m => m.ReadMorePageModule    ) },
  // { path: 'add-to-cart' , loadChildren: () => import('./add-to-cart/add-to-cart.module'  ).then( m => m.AddToCartPageModule   ) },
  // { path: 'show-product', loadChildren: () => import('./show-product/show-product.module').then( m => m.ShowProductPageModule ) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule { }
