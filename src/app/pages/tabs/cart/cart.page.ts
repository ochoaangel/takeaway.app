import { debounceTime } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import { LoadingOptions } from '@ionic/core';
import { Router } from '@angular/router';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyStorageService } from 'src/app/services/my-storage.service';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  // availableZones: any = [];
  constructor(
    public loadingController: LoadingController,
    private router: Router,
    public myV: MyVarService,
    public myS: MyStorageService,
    public myF: MyFunctionsService,
    private apiCall: ApiCallService,
    public platform: Platform
  ) { }

  loading = 0;
  panel = 'llevar'; // 'recoger' ||  'llevar

  paymentMethods; // métodos de pago, paypal, strype, efectivo
  productos; // coleccion solo de productos

  totalGeneral = 0;
  precioEnvio = 0;
  enableGeneral = false;
  enableCocina = false;

  dejarNota = '';
  dejarNombre = '';

  showDirectionsInputs = false;

  misDirecciones: any[];
  miDireccionSeleccionadaId;
  miDireccionSeleccionadaAll;

  enableDelivery = true;
  txtdisabledDelivery = 'Tienda INACTIVA en este momento';
  deliveriIdSelected: '';
  deliveryCompraMin = null;
  deliveryCode = null;
  deliveryCodeOld = null;
  deliveryDir1 = '';
  deliveryDir2 = '';

  uid_CART = '';
  uid_TKWPAYMETHOD = '';
  uid_PAYMENT = '';

  //  DVC88747356242796325961561194 / PM87549712766572802098682833 / 4090YKCFJRBV

  usuario = { nombre: '', correo: '', telefono: '' };
  effectiveVuelta = { bilete: null, vuelta: null };

  paymentSelected; // obj generado al presionar el boton de tipo de metodo de pago seleccionado

  ////////////////////////////////////////////////////////////////////////
  // ventanas o paneles
  ////////////////////////////////////////////////////////////////////////
  showIdentificarUsuario = false; // pedir nombre,correo y tlf
  showPaymentMethod = false; // decidir método de pago

  showEffective = false; // En caso de pagar efectivo pide monto y vuelta
  showConfirm = false; // en caso de confirm (para confirmar tipo de pago)


  showAddZone = false;

  userLogged = false; //

  DebounceBtnMas: Subject<any> = new Subject();
  DebounceBtnMenos: Subject<any> = new Subject();

  status = { general: false, delivery: false, cocina: false };


  aviableZones;

  ////////////////////////////////////
  // datos Importantes del formulario
  zonaSelected;
  calle = '';
  casa = '';

  ngOnInit() {
    this.DebounceBtnMas.pipe(debounceTime(500)).subscribe((params) => {
      this.apiCall.cartSyncDetails(params).subscribe((respSync) => {
        console.log('respSync=> ', respSync.ok, respSync.data.message);
        this.loadCart();
        this.actualizarCantidadCarrito();
      });
    });

    this.DebounceBtnMenos.pipe(debounceTime(500)).subscribe((params) => {
      this.apiCall.cartSyncDetails(params).subscribe((respSync) => {
        console.log('respSync=> ', respSync.ok, respSync.data.message);
        this.loadCart();
        this.actualizarCantidadCarrito();
      });
    });

    this.apiCall.CartPlacesGet().subscribe(zonasRecibidas => {
      console.log('zonasRecibidas', zonasRecibidas);
      if (zonasRecibidas.ok) {
        if (zonasRecibidas.data && zonasRecibidas.data.length > 0) {
          this.misDirecciones = zonasRecibidas.data
          this.miDireccionSeleccionadaAll = this.misDirecciones.filter(x => x.is_default === 1)[0];
          this.miDireccionSeleccionadaId = this.miDireccionSeleccionadaAll['id'];
          this.precioEnvio = this.miDireccionSeleccionadaAll.zone.recargo;
          this.actualizarTotal();
          console.log('this.miDireccionSeleccionadaAll', this.miDireccionSeleccionadaAll);

        } else {
          console.log('El usuario no tiene direcciones guardadas por ahora.');
        }
      } else {
        console.warn('ERROR al obtener zonas con: "CartPlacesGet()"');
      }
    })


    this.apiCall.aviableZones().subscribe(zonas => {
      if (zonas.ok) {
        this.aviableZones = zonas.data.zonas;
        console.log('this.aviableZones', this.aviableZones);
      } else {
        console.warn('ERROR al obteler aviableZones');
      }
    });


  }


  ionViewWillEnter() {
    this.initVars();

    if (this.myV.nowCordova) {
      console.log('--CORDOVAAAAAA');

      // caso Android e IOS

      if (this.myV.user && this.myV.user.uid) {
        console.log('this.myV.user', this.myV.user);
        this.userLogged = true;
        this.productos = null;
        this.loadCart();

        // consulto array para envíos
        this.loading++;
        this.apiCall.aviableZones().subscribe((infoZone) => {
          this.loading--;

          if (infoZone.ok) {
            // almacenando las operatividad
            const info = infoZone.data.isOperative;
            this.status.general = info.general.status;
            this.status.cocina = info.cocina.status;
            this.status.delivery = info.delivery.status;
            //////////////////////////////////////////////
            console.log('status:', this.status);

            if (this.status.general) {
              // this.evaluateZone();
              if (!(this.productos && this.productos.length === 0)) {

                // hay delivery o cocina
                if (this.status.delivery || this.status.cocina) {
                  ///////////////////////////////////////////////////////////////////////////////////
                  // consultar metodos de pago
                  this.apiCall.aviablePaymentMethods().subscribe((resp) => {
                    if (resp.data) {
                      this.paymentMethods = resp.data;
                      console.log('this.paymentMethods', this.paymentMethods);
                    } else {
                      console.warn(
                        'ERROR, no se pudo Obtener métodos de pagos..'
                      );
                    }
                  });
                }

                if (!this.status.delivery && !this.status.cocina) {
                  this.myF.showAlertOk(
                    'Notificación',
                    'info',
                    info.general.message ||
                    'En estos momentos nuestro restaurant no se encuentra disponible para procesar su pedido, intente más tarde.. '
                  );
                }
              }
            } else {
              this.myF.showAlertOk(
                'Notificación',
                'info',
                info.general.message ||
                'En estos momentos nuestro restaurant no se encuentra disponible para procesar su pedido, intente más tarde.. '
              );
            }

            if (!this.status.delivery && this.status.cocina) {
              this.panel = 'recoger';
            }

          } else {
            this.myF.showAlertOk(
              'Notificación',
              'info',
              'Tenenos inconvenientes para ingresar al carrito, intente mas trade..'
            );
          }
        });
      } else {
        // caso de usuario no registrado
        this.myF.showAlertOk(
          'Notificación',
          'info',
          'Para Acceder al Carrito debe estar registrado..'
        );
        this.router.navigateByUrl('/tabs/account');
        this.userLogged = false;
      }
    } else {
      // caso WEB
      console.log('--WEBBBBBBBBBBB');

      this.userLogged = true;
      this.productos = null;
      this.loadCart();
      this.panel = 'recoger';

      // consulto array
      if (!(this.productos && this.productos.length === 0)) {
        // consultar metodos de pago
        this.apiCall.aviablePaymentMethods().subscribe((resp) => {
          if (resp.data) {
            this.paymentMethods = resp.data;
            console.log('this.paymentMethods', this.paymentMethods);
          } else {
            console.warn('ERROR, no se pudo Obtener métodos de pagos..');
          }
        });
      }

      // this.deliveryZones = null;
    }
  }

  ionViewDidLeave() {
    console.log('Ya salió');
    this.productos = null;
  }

  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  // Funciones  Generales

  actualizarTotal() {
    this.totalGeneral = 0;
    this.productos.forEach((producto) => {
      this.totalGeneral =
        this.totalGeneral + producto.cantidad * producto.precio;
    });
    this.totalGeneral = this.totalGeneral + this.precioEnvio;
  }

  actualizarCantidadCarrito() {
    this.apiCall.cartCount().subscribe((cantidad) => {
      this.myV.cantidadCarrito = cantidad.data.count;
      this.myV.carritoActualizar(this.myV.cantidadCarrito);
    });
  }

  ocultarTodasLasVentanas() {
    this.showIdentificarUsuario = false;
    this.showPaymentMethod = false;
    this.showEffective = false;
    this.showConfirm = false;
    this.showAddZone = false;
  }


  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  // BOTONES

  ////////////////////////////////////////////////////////////////////////
  // productos del carrito carrito
  btnMas(id, nProducto) {
    this.productos[nProducto].cantidad++;
    const cantidad = this.productos[nProducto].cantidad;
    const params = { items: [{ id, cantidad }] };
    this.DebounceBtnMas.next(params);
  }

  btnMenos(id, nProducto) {
    if (this.productos[nProducto].cantidad > 1) {
      this.productos[nProducto].cantidad--;
      const cantidad = this.productos[nProducto].cantidad;
      const params = { items: [{ id, cantidad }] };
      this.DebounceBtnMenos.next(params);
    }
  }

  btnEliminar(id, nProducto) {
    this.apiCall.cartRem({ dvcartitem_id: id }).subscribe((resp) => {
      this.loadCart();
      if (resp && resp.ok) {
        this.actualizarCantidadCarrito();
        if (this.productos.length === 0) {
          this.router.navigateByUrl('/tabs/menu');
        }
      } else {
        console.warn('ERROR, al eliminar elemento del carrito..');
      }
    });
  }

  btnLlevar() {
    this.panel = 'llevar';
    // this.cambioDeZonaDelivery();
    console.log('Presionado btnLlevar ');
  }

  btnRecoger() {
    this.panel = 'recoger';
    this.precioEnvio = 0;
    this.actualizarTotal();
  }

  btnPagar() {

    if (!this.enableDelivery) {
      console.warn('ERROR, tienda inhabilitada enableDelivery=false');
      this.myF.showAlertOk('Notificación', 'info', 'Tienda inhabilidada..');
    } else if (this.panel === 'llevar' && !this.miDireccionSeleccionadaId) {
      console.log('Debe elegir una zona para envío');
      this.myF.showAlertOk(
        'Notificación',
        'info',
        'Debe elegir una zona para envío..'
      );
    } else if (
      this.panel === 'recoger' &&
      this.myV.intoLocalMode === 'autoServicio'
    ) {
      // caso  AUTOSERVICIO

      let params = { delivery: { tkwrestzone_id: null } };
      if (this.dejarNota) {
        params.delivery['nota'] = this.dejarNota;
      }
      if (this.dejarNombre) {
        params.delivery['nombre_cliente'] = this.dejarNombre;
      }

      console.log('paramsParaAutoServicio', params);

      this.apiCall.cartSyncDetails(params).subscribe((respSync) => {
        console.log('respSync en AutoServicio', respSync);
        this.ocultarTodasLasVentanas();
        this.showPaymentMethod = true;
      });
    } else {
      let params;
      if (this.panel === 'llevar') {
        params = {
          delivery: {
            tkwrestzone_id: this.miDireccionSeleccionadaId,
            dir: this.miDireccionSeleccionadaAll.dir,
            dir2: this.miDireccionSeleccionadaAll.dir2,
          },
        };

        if (this.dejarNota) {
          params.delivery['nota'] = this.dejarNota;
        }

        if (this.myV.user.nombre) {
          params.delivery['nombre_cliente'] = this.myV.user.nombre;
        }

        this.apiCall.cartSyncDetails(params).subscribe((respSync) => {
          console.log('respSync', respSync);

          if (this.userLogged) {
            this.ocultarTodasLasVentanas();
            this.showPaymentMethod = true;
          } else {
            this.ocultarTodasLasVentanas();
            this.showIdentificarUsuario = true;
          }
        });
      } else {
        params = {
          delivery: {
            tkwrestzone_id: null,
          },
        };

        if (this.dejarNota) {
          params.delivery.nota = this.dejarNota;
        }

        if (this.myV.user.nombre) {
          params.delivery['nombre_cliente'] = this.myV.user.nombre;
        }

        if (this.userLogged) {
          this.ocultarTodasLasVentanas();
          this.showPaymentMethod = true;
        } else {
          this.ocultarTodasLasVentanas();
          this.showIdentificarUsuario = true;
        }
      }
    }

  }

  btnPaymentMethodSelected(details) {
    this.paymentSelected = details;

    switch (details.type) {
      case 'gateway':
        console.log('Entrando en metodo de pago GATEWAY');
        this.ocultarTodasLasVentanas();
        this.myV.initPayment = details.urls;
        this.router.navigateByUrl('/payment');
        break;

      case 'confirm':
        this.ocultarTodasLasVentanas();
        this.showConfirm = true;
        break;

      case 'effective':
        console.log('Entrando en metodo de pago EFFECTIVE');
        this.ocultarTodasLasVentanas();
        this.showEffective = true;
        break;

      default:
        console.warn(
          'ERROR, tipo de método de pago Desconocido.. verificar->',
          details
        );
        break;
    }
  }

  ////////////////////////////////////////////////////////////////////////
  /////////////////////////////// VENTANAS ///////////////////////////////
  ////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////
  // ventana Identificar Usuario

  btnUserVolver() {
    this.ocultarTodasLasVentanas();
    this.usuario.nombre = '';
    this.usuario.telefono = '';
    this.usuario.correo = '';
  }

  btnUserContinuar() {
    this.ocultarTodasLasVentanas();
    if (this.usuario.nombre === '') {
      console.log('Debe ingresar un Nombre');
      this.myF.showAlertOk('Notificación', 'info', 'Debe ingresar un Nombre..');
    } else if (this.usuario.telefono === '') {
      console.log('Debe ingresar un Teléfono');
      this.myF.showAlertOk(
        'Notificación',
        'info',
        'Debe ingresar un Teléfono..'
      );
    } else if (this.usuario.correo === '') {
      console.log('Debe ingresar un correo');
      this.myF.showAlertOk('Notificación', 'info', 'Debe ingresar un correo..');
    } else {
      console.log('Ingresó todos los datos de usuario..');

      const params = {
        nombre: this.usuario.nombre,
        email: this.usuario.correo,
        tel: this.usuario.telefono,
      };
      this.apiCall.clienRegisterAnom(params).subscribe((respUsuario) => {
        if (respUsuario.ok) {
          console.log(
            'respUsuario.data.client_uid',
            respUsuario.data.client_uid
          );
          this.ocultarTodasLasVentanas();
          this.showPaymentMethod = true;
        } else {
          console.log('ERROR, al registrar usuario', this.usuario);
          this.myF.showAlertOk('Error', 'info', 'Error al registrar Usuario..');
        }
      });
    }
  }

  ////////////////////////////////////////////////////////////////////////
  // ventana paymentMethods
  btnPaymentMethodVolver() {
    this.ocultarTodasLasVentanas();
    if (!this.userLogged) {
      this.showIdentificarUsuario = true;
    }
  }

  btnPaymentMethodContinuar() {
    this.ocultarTodasLasVentanas();
    this.showEffective = true;
  }

  ////////////////////////////////////////////////////////////////////////
  // Ventana Effective
  btnEffeciveVolver() {
    this.ocultarTodasLasVentanas();
    this.showPaymentMethod = true;
  }

  btnEffeciveContinuar() {
    if (this.effectiveVuelta.vuelta < 0) {
      this.myF.showAlertOk('Notificación', 'info', 'Ingrese un monto válido.');
    } else if (
      this.effectiveVuelta.vuelta > 0 &&
      this.effectiveVuelta.vuelta < this.totalGeneral
    ) {
      this.myF.showAlertOk(
        'Notificación',
        'info',
        'La cantidad a pagar con Efectivo debe ser igual o mayor que el monto necesario.'
      );
    } else if (this.effectiveVuelta.vuelta - this.totalGeneral > 500) {
      this.myF.showAlertOk(
        'Notificación',
        'info',
        'Ingrese un monto válido, mas cercano al monto necesario.'
      );
    } else {
      this.notificarMétodoPagoDefinitivo(
        this.paymentSelected.id,
        this.effectiveVuelta.vuelta
      );
    }
  }

  ////////////////////////////////////////////////////////////////////////
  // Ventana Confirm

  btnConfirmVolver() {
    this.ocultarTodasLasVentanas();
    this.showPaymentMethod = true;
  }

  btnConfirmContinuar() {
    this.ocultarTodasLasVentanas();
    this.notificarMétodoPagoDefinitivo(this.paymentSelected.id);
  }

  notificarMétodoPagoDefinitivo(id, vuelta?) {
    let params = { id };
    vuelta ? (params['vuelta'] = vuelta) : null;

    console.log('params A Enviar', params);
    this.apiCall.payWith(params).subscribe((respuesta) => {
      if (respuesta.ok) {
        this.effectiveVuelta.vuelta = null;
        this.ocultarTodasLasVentanas();
        this.myV.carritoActualizar(0);
        this.router.navigateByUrl('/tabs/home');
        this.myF.showAlertOk(
          'Notificación',
          'done',
          'Excelente..</br> Compra Notificada Exitosamente..'
        );
      } else {
        this.myF.showAlertOk(
          'Notificación',
          'info',
          'Error.. hubo problemas al notificar el pago..'
        );
      }
    });
  }

  initVars() {
    this.loading = 0;
    this.panel = 'llevar'; // 'recoger' ||  'llevar
    this.paymentMethods = null; // métodos de pago, paypal, strype, efectivo
    this.productos = null; // coleccion solo de productos
    this.totalGeneral = 0;
    this.precioEnvio = 0;
    this.misDirecciones = null;
    this.miDireccionSeleccionadaId = '';
    this.miDireccionSeleccionadaId = null;
    this.enableGeneral = false;
    this.showDirectionsInputs = false;
    this.enableDelivery = true;
    this.deliveriIdSelected = '';
    this.deliveryCode = null;
    this.deliveryCodeOld = null;
    this.deliveryDir1 = '';
    this.deliveryDir2 = '';
    this.uid_CART = '';
    this.uid_TKWPAYMETHOD = '';
    this.uid_PAYMENT = '';
    this.usuario = { nombre: '', correo: '', telefono: '' };
    this.effectiveVuelta = { bilete: null, vuelta: null };
    this.paymentSelected = null; // obj generado al presionar el boton de tipo de metodo de pago seleccionado
    ////////////////////////////////////////////////////////////////////////
    // ventanas o paneles
    ////////////////////////////////////////////////////////////////////////
    this.showIdentificarUsuario = false; // pedir nombre,correo y tlf
    this.showPaymentMethod = false; // decidir método de pago
    this.showEffective = false; // En caso de pagar efectivo pide monto y vuelta
    this.showConfirm = false; // en caso de confirm (para confirmar tipo de pago)
    this.userLogged = false; //
  }

  loadCart() {
    // Buscando Productos del carrito..'
    this.loading++;
    this.apiCall.cart().subscribe((carrito) => {
      console.log('carrito', carrito);
      this.loading--;
      if (carrito && carrito.ok) {
        this.productos = carrito.data.items;
        this.uid_CART = carrito.data.uid;
        this.totalGeneral = carrito.data.total;
        this.actualizarTotal();
      } else {
        this.productos = [];
        console.warn(
          'ERROR, al obtener el contenido del carrito desde el servidor..'
        );
      }
    });
  }

  cambioDeSeleccionDeZona() {
    this.miDireccionSeleccionadaAll = this.misDirecciones.filter(x => x.id === this.miDireccionSeleccionadaId)[0];
    this.precioEnvio = this.miDireccionSeleccionadaAll.zone.recargo;
    this.actualizarTotal();
  }

  btnAddZone() {
    if (!this.zonaSelected) {
      this.myF.showAlertOk('Notificación', 'info', 'Debe Seleccionar una Zóna válida para continuar..');
    } else if (this.calle.trim().length < 3) {
      this.myF.showAlertOk('Notificación', 'info', 'Debe Ingresar una <br>Calle/Avenida/Acera/Carretera<br> válida para continuar..');
    } else if (this.casa.trim().length < 1) {
      this.myF.showAlertOk('Notificación', 'info', 'Debe Ingresar una <br>Casa/Apartamento/Portal<br> válido para continuar..');
    } else {

      let end = {
        tkwrestzone_id: Number(this.zonaSelected),
        dir: this.calle.trim(),
        dir2: this.casa.trim(),
        default: true
      }

      this.apiCall.CartPlacesAdd(end).subscribe(resp => {
        console.log('resp', resp);
        if (resp.ok) {
          this.myF.showAlertOk('Éxito', 'done', 'Hemos agregado exitosamente una nueva ubicación');
          this.ocultarTodasLasVentanas()
          this.ngOnInit()
        } else {
          this.myF.showAlertOk('Notificación', 'info', 'Tuvimos inconvenientes para agregar la ubicación, intente nuevamente..');
          console.warn('ERROR al agregar nueva ubicación usando CartPlacesAdd');
        }
      })
    }
  }

  btnVolver() {
    console.log('BtnVolver');
    // this.router.navigateByUrl('/tabs/cart');
    // this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
    // this.router.navigate(['/tabs/cart']).then(() => window.location.reload());
  }


  deleteDireccion(position) {
    this.apiCall.CartPlacesDelete(position).subscribe(resp => {
      if (resp.ok) {
        this.misDirecciones.splice(position, 1);
      } else {
        console.warn('Error al Eliminar miDireccion con CartPlacesDelete() ');
      }
    })
  }

}
