import { Component, OnInit, enableProdMode } from "@angular/core";
import { ApiCallService } from "src/app/services/api-call.service";
import { MyVarService } from "src/app/services/my-var.service";
import { Router } from "@angular/router";
import * as _ from "underscore";
import { MyFunctionsService } from "src/app/services/my-functions.service";
import { MyStorageService } from "src/app/services/my-storage.service";
import { NavController, Platform } from "@ionic/angular";
import { Subscription } from 'rxjs';

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  ///////////////////////////////////////////////////////////
  // sliders OPTIONS
  optionsSlideDestaque = {
    slidesPerView: 1,
    freeMode: false,
  };

  optionsSlidePromociones = {
    slidesPerView: 3.5,
    spaceBetween: 10,
  };

  optionsSlideCategorias = {
    initialSlide: 1,
    slidesPerView: 3.2,
    loop: false,
    centeredSlides: false,
  };

  ///////////////////////////////////////////////////////////
  // arrays para listas
  // categorias;
  // productos;
  // popularidades;

  ///////////////////////////////////////////////////////////
  // variables usadas
  segment_select = "carta";
  searchText = "";
  searchResult = [];
  estructuraHome;
  child: any;
  subscription: any;
  private backButtonSub: Subscription;


  constructor(
    public myV: MyVarService,
    public myS: MyStorageService,
    private myF: MyFunctionsService,
    private apiCall: ApiCallService,
    private router: Router,
    private plt: Platform,
    private navCtrl: NavController,

  ) { }


  ngOnInit() {
    this.apiCall.getNewHome().subscribe((estructuraHome) => {
      console.log("estructuraHome", estructuraHome);
      if (estructuraHome.ok) {
        this.estructuraHome = estructuraHome.data;
      } else {
        console.error("ERROR: datos del home no obtenidos del Servidor");
      }
    });
    this.myV.avoidSplash = true;
  }

  
  ionViewWillEnter() {
    this.myF.updateProductsAndCategories();
    this.subscription = this.plt.backButton.subscribeWithPriority(9999, () => {
      // do nothing
      localStorage.removeItem("appZoneSelected");
      this.navCtrl.pop();
    });
  }

  ionViewDidEnter() {
    this.backButtonSub = this.plt.backButton.subscribeWithPriority(
      10000,
      () => {
        this.myF.actualizarUid();
        this.router.navigateByUrl('/init-with-children')
      }
    );
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  updateSearch(textoAbuscar: string) {
    this.searchResult = this.myF.searchingText(textoAbuscar);
    console.log("searchResult", this.searchResult);
    // console.log('this.searchText', this.searchText);
  }

  mostrarProducto(id) {
    const product = this.myV.products.filter((x) => x.prodId === id)[0];
    if (product) {
      this.myV.initShowProduct = id;
      this.router.navigateByUrl("/tabs/menu/show-product");
    } else {
      console.warn(
        "El ID del Producto seleccionado, no se encuentra en la lista de productos.."
      );
    }
  }

  mostrarCategoria(id) {
    console.log("catId", id);
    this.myV.initMenuCategoryId = id;
    this.router.navigateByUrl("/tabs/menu");
  }

  clickLinkAction(action, href, productId) {
    if (action === "internal") {
      console.log("internal", action, href);

      switch (href) {
        case "/tabs/menu/show-product":
          this.mostrarProducto(productId);
          break;

        case "/ir-a-registrarse":
          this.router.navigateByUrl("/tabs/account");
          break;

        default:
          this.router.navigateByUrl(href);
          break;
      }
    } else {
      console.log("External", action, href);

      if (this.plt.is("cordova")) {
        this.myV.initExternal = href;
        this.router.navigateByUrl("/external");
      } else {
        window.open(href, "_blank");
      }
    }
  }

  doRefresh(event) {
    this.apiCall.getHome().subscribe((estructuraHome) => {
      console.log("estructuraHome", estructuraHome);
      if (estructuraHome.ok) {
        this.estructuraHome = estructuraHome.data;
        event.target.complete();
      } else {
        event.target.complete();
        console.error("ERROR: datos del home no obtenidos del Servidor");
      }
    });
  }
} // fin clase ppal
