import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { filter } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Platform } from "@ionic/angular";
// import { Storage } from '@ionic/storage';
import { DOCUMENT } from "@angular/common";
import { darkStyle } from "./map-dark-style";
declare var google;

@Component({
  selector: "app-tracking",
  templateUrl: "./tracking.page.html",
  styleUrls: ["./tracking.page.scss"],
})
export class TrackingPage implements OnInit {
  @ViewChild("map", { static: true }) mapElement: ElementRef;

  map: any;
  currentMapTrack = null;

  isTracking = false;
  trackedRoute = [];
  previousTracks = [];

  positionSubscription: Subscription;
  constructor(
    private plt: Platform,
    private geolocation: Geolocation,
    // private storage: Storage,
    @Inject(DOCUMENT) private doc: Document
  ) {}

  ngOnInit() {
    this.loadHistoricRoutes();

    const appEl = this.doc.querySelector("ion-app");
    let isDark = false;
    let style = [];
    if (appEl.classList.contains("dark-theme")) {
      style = darkStyle;
    }

    let mapOptions = {
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.geolocation
      .getCurrentPosition()
      .then((pos) => {
        let latLng = new google.maps.LatLng(
          pos.coords.latitude,
          pos.coords.longitude
        );
        this.map.setCenter(latLng);
        this.map.setZoom(16);
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });

    const observer = new MutationObserver(function (mutations) {
      mutations.forEach((mutation) => {
        if (mutation.attributeName === "class") {
          const el = mutation.target as HTMLElement;
          isDark = el.classList.contains("dark-theme");
          if (this.map && isDark) {
            this.map.setOptions({ styles: darkStyle });
          } else if (this.map) {
            this.map.setOptions({ styles: [] });
          }
        }
      });
    });

    observer.observe(appEl, {
      attributes: true,
    });
  }

  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];
    this.positionSubscription = this.geolocation
      .watchPosition()
      .pipe(filter((p) => p["coords"] !== undefined)) //Filter Out Errors
      .subscribe((data) => {
        setTimeout(() => {
          this.trackedRoute.push({
            lat: data["coords"].latitude,
            lng: data["coords"].longitude,
          });
          this.redrawPath(this.trackedRoute);
        }, 0);
      });
  }

  redrawPath(path) {
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: "#ff00ff",
        strokeOpacity: 1.0,
        strokeWeight: 3,
      });
      this.currentMapTrack.setMap(this.map);
    }
  }

  stopTracking() {
    let newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
    this.previousTracks.push(newRoute);
    // this.storage.set('routes', this.previousTracks);

    this.isTracking = false;
    this.positionSubscription.unsubscribe();
    this.currentMapTrack.setMap(null);
  }

  showHistoryRoute(route) {
    this.redrawPath(route);
  }

  loadHistoricRoutes() {
    // this.storage.get('routes').then(data => {
    //   if (data) {
    //     this.previousTracks = data;
    //   }
    // });
  }
}
