import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitIntoLocalPage } from './init-into-local.page';

const routes: Routes = [
  {
    path: '',
    component: InitIntoLocalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InitIntoLocalPageRoutingModule {}
