import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InitIntoLocalPageRoutingModule } from './init-into-local-routing.module';

import { InitIntoLocalPage } from './init-into-local.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InitIntoLocalPageRoutingModule
  ],
  declarations: [InitIntoLocalPage]
})
export class InitIntoLocalPageModule {}
