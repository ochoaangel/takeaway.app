import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { MyVarService } from 'src/app/services/my-var.service';

@Component({
  selector: 'app-init-into-local',
  templateUrl: './init-into-local.page.html',
  styleUrls: ['./init-into-local.page.scss'],
})
export class InitIntoLocalPage implements OnInit {

  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    public router: Router,
  ) { }

  ngOnInit() {
    console.log('myV', this.myV.appInfo);
  }

  btnAutoServicio() {
    this.myV.intoLocalMode = 'autoServicio';
    this.router.navigateByUrl('/tabs/home');
  }

  btnLlamarCamarero() {
    this.myV.intoLocalMode = 'llamarCamarero';
    this.router.navigateByUrl('/tabs/home');
  }


}
