import { GeolocationService } from './../../services/geolocation.service';
import { Component, OnInit } from '@angular/core';
import { ApiCallService } from '../../services/api-call.service';
import { MyVarService } from '../../services/my-var.service';
import {
  AlertController,
  IonImg,
  LoadingController,
  NavController,
  Platform,
  ToastController,
} from '@ionic/angular';
import { MyStorageService } from '../../services/my-storage.service';
import { AlertInput } from '@ionic/core';
import { InitConfigService } from '../../services/init-config.service';
import { Title } from '@angular/platform-browser';
import { MyFunctionsService } from '../../services/my-functions.service';
import { Router } from '@angular/router';
import {
  InAppBrowser,
  InAppBrowserOptions,
  InAppBrowserEvent,
} from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-init-with-children',
  templateUrl: './init-with-children.page.html',
  styleUrls: ['./init-with-children.page.scss'],
})
export class InitWithChildrenPage implements OnInit {
  children: any = [];
  zones: any[] = [];
  zone: any;
  appIcon: HTMLLinkElement = document.querySelector('#appIcon');
  loading: any;
  styles: any = [];
  rest: any = [];
  childSelected: any;
  res: boolean = true;
  oneTime: any = false;
  homeData: any = [];
  optionsSlidePromociones = {
    slidesPerView: 3.5,
    spaceBetween: 10,
  };

  urlExternal;
  inAppBrowserRef;

  options: InAppBrowserOptions = {
    location: 'no', //Or 'no'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    beforeload: 'yes',
  };
  constructor(
    private apiCallService: ApiCallService,
    public myV: MyVarService,
    private myS: MyStorageService,
    private myF: MyFunctionsService,
    private alertController: AlertController,
    private apiCall: ApiCallService,
    private titleService: Title,
    private loadingController: LoadingController,
    private initConfigService: InitConfigService,
    private toastController: ToastController,
    private navCtrl: NavController,
    private geoS: GeolocationService,
    private router: Router,
    private plt: Platform,
    private iab: InAppBrowser
  ) { }

  ngOnInit() { }
  ionViewDidEnter() { console.warn('>>> SE HA INGRESADO COMPLETAMENTE A INITWITHCHILDREN'); }

  ionViewWillEnter() {

    console.log('>>>>ionViewWillEnter InitWithChildren********');

    if (!this.myV.uid) {
      this.myF.actualizarUid();
    }
    this.setStyles();
    this.getZones();
    this.initGeocode();
  }

  getHome() {
    this.apiCall.getNewHome().subscribe((estructuraHome) => {
      console.log('estructuraHome', estructuraHome);
      if (estructuraHome.ok) {
        this.homeData = estructuraHome['data'];
      } else {
        console.error('ERROR: datos del home no obtenidos del Servidor');
      }
    });
  }

  mostrarProducto(id) {
    console.log('catId', id);
    this.myV.initMenuCategoryId = id;

    this.apiCall.categories({ include: 'products' }).subscribe(async (cat) => {
      // preparo todos los productos a mi manera
      this.myV.products = this.myF.prepareProducts(cat.data);

      // preparo solo categorias
      this.myV.categories = this.myF.getUnique(this.myV.products, 'catId');

      const product = this.myV.products.filter((x) => x.prodId === id)[0];
      if (product) {
        this.myV.initShowProduct = id;
        this.router.navigateByUrl('/tabs/menu/show-product');
      } else {
        const toast = await this.toastController.create({
          header: 'Oh oh!',
          message:
            'El ID del Producto seleccionado, no se encuentra en la lista de productos..',
          duration: 5000,
        });
        await toast.present();
        console.warn(
          'El ID del Producto seleccionado, no se encuentra en la lista de productos..'
        );
      }
    });
  }

  clickLinkAction(action, href, categoryId, child, product_id) {
    if (action === 'internal') {
      console.log('internal', action, href);

      switch (href) {
        case '/tabs/menu':
          {
            this.mostrarCategoria(categoryId, child);
          }
          break;

        case '/tabs/menu/show-product':
          this.mostrarProducto(product_id);

          break;

        case '/ir-a-registrarse':
          this.router.navigateByUrl('/tabs/account');
          break;

        default:
          this.router.navigateByUrl(href);
          break;
      }
    } else {
      console.log('External', action, href);

      if (this.plt.is('cordova')) {
        this.myV.initExternal = href;
        this.OpenExternal();
      } else {
        window.open(href, '_blank');
      }
    }
  }

  mostrarCategoria(id, child) {
    console.log('catId', id);
    this.myV.initMenuCategoryId = id;

    this.apiCall.categories({ include: 'products' }).subscribe((cat) => {
      // preparo todos los productos a mi manera
      this.myV.products = this.myF.prepareProducts(cat.data);

      // preparo solo categorias
      this.myV.categories = this.myF.getUnique(this.myV.products, 'catId');
      console.log(this.myV.categories);
      this.openRestaurant(child);
      this.router.navigateByUrl('/tabs/menu');
    });
  }

  OpenExternal() {
    this.urlExternal = this.myV.initExternal;
    this.inAppBrowserRef = this.iab.create(this.urlExternal, '_blank');
    this.inAppBrowserRef.on('exit').subscribe((event) => { this.myF.actualizarUid() });
  }

  initGeocode() {
    this.geoS.locationStatus().then(
      () => {
        this.geoS.checkLocationEnabled().then(
          () => {
            console.log('LISTO');
          },
          (err) => {
            console.log(err);
          }
        );
      },
      (err) => {
        console.log('Permisos rechazados', err);
      }
    );
  }

  private getChildren() {
    this.oneTime = false;
    this.apiCallService.getChildren().subscribe(async (res) => {
      console.log('-> res', res);
      this.myS.remove('childSelected');
      if (res.ok) {
        this.children = res.data.children;
        console.log('this.children', this.children);
      } else {
        const toast = await this.toastController.create({
          header: 'Oh oh!',
          message: res.data.message || 'Ha ocurrido un problema',
          duration: 5000,
        });
        await toast.present();
        this.getConfig();
      }
    });
  }

  private getZones() {
    this.apiCallService.getZones().subscribe(async (res) => {
      console.log('-> res', res);
      if (res.ok) {
        this.zones = res.data.zones;
        this.getLocation();
      } else {
        this.presentToast(res.data.message);
      }
    });
  }

  styleObject(): Object {
    return {
      'background-color': localStorage.getItem('firstRestColor') + '!important',
      color: 'white',
    };
  }

  private setStyles() {
    // color primario para el fondo de la lista
    if (!localStorage.getItem('firstRestColor')) {
      this.styles.background_list = JSON.parse(
        localStorage.getItem('appInitConfig')
      );
      if (this.styles.background_list) {
        this.styles.background_list = this.styles.background_list.config.colors;
        const myColors = this.styles.background_list.trim().split(';');
        localStorage.setItem(
          'firstRestColor',
          myColors[0].split(':')[1].toString()
        );
      }
    }

    // Fuente seleccionada para labels
    this.styles.font_style = JSON.parse(localStorage.getItem('appInitConfig'));
    if (this.styles.font_style) {
      this.styles.font_style = this.styles.font_style['fonts'].title;
    }
  }

  async openRestaurant(child: any) {
    this.myS.set('firstRest', JSON.stringify(this.styles)).subscribe(() => { });
    this.myF.actualizarUid(child.uid);
    this.myS.set('childSelected', child).subscribe((res) => {
      if (res) {
        this.getConfig();
        this.navCtrl.navigateForward('/tabs/home');
        console.log('-> child', child);
        this.childSelected = child;
        this.childSelected.pivot.json_sections_before = this.rest;
      } else {
        console.warn('*****HIJO NO GUARDADO EN EL STORAGE');
      }
    });
  }

  async localitySelectionAlert() {
    const alert = await this.alertController.create({
      cssClass: 'alert-location-select',
      header: 'Selecciona tu zona por favor',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Seleccionar localidad',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.selectZone();
          },
        },
        {
          text: 'Usar mi ubicación',
          handler: () => {
            this.geoS.comparePostalCode(this.zones).then(
              (res) => {
                this.setLocation(res);
              },
              (err) => {
                this.res = false;
                console.log(this.children.length);
                this.zone = err;
              }
            );
          },
        },
      ],
    });
    if (!this.oneTime) {
      this.oneTime = true;
      await alert.present();
    }
  }

  async selectZone() {

    const inputsZone: AlertInput[] = [];
    this.zones.forEach((zone, id) => {
      inputsZone.push({
        name: `zone${id}`,
        type: 'radio',
        label: `${zone.code}: ${zone.nombre}, ${zone.ciudad}`,
        value: zone,
      });
    });
    const alert = await this.alertController.create({
      cssClass: 'alert-width-90',
      header: 'Selecciona tu zona por favor',
      backdropDismiss: false,
      inputs: inputsZone,
      buttons: [
        {
          text: 'Seleccionar',
          handler: (data) => {
            if (!data) {
              this.presentToast('Es necesario que selecciones tu zona');
              this.selectZone();
            } else {
              this.setLocation(data);
            }
            console.log('Confirm Ok');
            console.log('-> data', data);
          },
        },
      ],
    });

    await alert.present();
  }

  private setLocation(zone: any) {
    this.zone = zone;
    this.myV.postalCode = zone.code;
    this.res = true;
    this.myS
      .set('appZoneSelected', zone)
      .subscribe((x) => console.log('Zona guardada con exito -> ', x));
    this.getChildren();
  }

  doRefresh(event) {
    this.apiCallService.getChildren().subscribe(async (res) => {
      console.log('-> res', res);
      event.target.complete();
      if (res.ok) {
        this.children = res.data.children;
        console.log('this.children', this.children);
      } else {
        const toast = await this.toastController.create({
          header: 'Oh oh!',
          message: res.data.message || 'Ha ocurrido un problema',
          duration: 5000,
        });
        await toast.present();
        this.getConfig();
      }
    });
  }

  private getLocation() {
    this.myS.get('appZoneSelected').subscribe((x) => {
      if (x) {
        this.setLocation(x);
      } else {
        this.localitySelectionAlert();
      }
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      header: 'Oh oh!',
      message: message || 'Ha ocurrido un problema',
      duration: 3000,
    });
    await toast.present();
  }

  setStatus(status: string) {
    switch (status) {
      case 'open':
        return { name: 'Abierto', color: 'success' };
      case 'closeSoon':
        return { name: 'Cierra pronto', color: 'warning' };
      case 'close':
        return { name: 'Cerrado', color: 'danger' };
      default:
        return { name: 'Sin horario', color: 'tertiary' };
    }
  }

  setMode(mode: 'A' | 'D' | 'L') {
    switch (mode) {
      case 'L':
        return { name: 'SOLO LOCAL/RECOGIDA', icon: '/assets/icon/bag.svg' };
      case 'D':
        return {
          name: 'SOLO DELIVERY',
          icon: '/assets/icon/delivery-motorbike.svg',
        };
      default:
        return {
          name: 'COMER AQUI, TAMBIEN A DOMICILIO',
          icon: '/assets/icon/restaurant.svg',
        };
    }
  }

  private getConfig() {
    this.presentLoading();
    this.apiCall.config({ config_uid: this.myF.getUID() }).subscribe((apiConfig) => {
      this.loading.dismiss();
      this.rest = apiConfig;
      const preparado = this.initConfigService.prepareConfig(apiConfig);
      this.initConfigService.setConfig(preparado);

      // colocandole titulo a la pestaña Web General
      this.titleService.setTitle(this.myV.appInfo.nombre);
      // colocando icono a la WEB
      this.appIcon.href = this.myV.urlBaseImg + this.myV.appInfo.brand_logo;

      this.myS.get('user').subscribe((respUser) => {
        if (respUser) {
          this.myV.user = respUser;
          console.log('*****USUARIO LOGUEADO EN EL STORAGE');
        } else {
          console.warn('*****USUARIO NO LOGUEADO EN EL STORAGE');
        }
      });
      this.myS.set('childSelected', this.childSelected).subscribe((res) => {
        if (res) {
          console.log('*****HIJO GUARDADO EN EL STORAGE');
        } else {
          console.warn('*****HIJO NO GUARDADO EN EL STORAGE');
        }
      });

      //////////////////////////////////////////////////////////////////////////////////
      // definir el modo de inicio
      /*  this.router.navigateByUrl("/tabs/home");
      this.navCtrl.navigateForward("/tabs/home"); */
    });
  }

  private async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Espere mientras esta siendo redirigido...',
    });
    await this.loading.present();
  }

  view(img: IonImg) {
    console.log('-> img', img);
  }
}
