import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitWithChildrenPage } from './init-with-children.page';

const routes: Routes = [
  {
    path: '',
    component: InitWithChildrenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InitWithChildrenPageRoutingModule {}
