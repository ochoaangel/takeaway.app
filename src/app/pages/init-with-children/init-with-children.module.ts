import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InitWithChildrenPageRoutingModule } from './init-with-children-routing.module';

import { InitWithChildrenPage } from './init-with-children.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        InitWithChildrenPageRoutingModule,
        ComponentsModule,
    ],
  declarations: [InitWithChildrenPage]
})
export class InitWithChildrenPageModule {}
