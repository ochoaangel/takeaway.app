import { Component, OnInit } from '@angular/core';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions, InAppBrowserEvent } from '@ionic-native/in-app-browser/ngx';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { Platform } from '@ionic/angular';
import * as moment from 'moment';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  urlPay;
  inAppBrowserRef;
  load = true;

  loopTimeCheck = 5000;
  timeControl = null;

  options: InAppBrowserOptions = {
    location: 'no',
    clearcache: 'yes',
    clearsessioncache: 'yes',
    beforeload: 'yes',
  };


  constructor(
    private myV: MyVarService,
    private myF: MyFunctionsService,
    private apiCall: ApiCallService,
    private router: Router,
    private platform: Platform,
    private iab: InAppBrowser
  ) { }


  ngOnInit() {
    console.log('Ingresando en pagina PAYMENT');

    // lectura de variables(url) necesarias
    this.urlPay = this.myV.initPayment;
    console.log('this.urlPay', this.urlPay);

    if (this.myV.nowCordova) {
      // caso cordova
      this.inAppBrowserRef = this.iab.create(this.urlPay.launch, '_blank');
      this.inAppBrowserRef.on('loadstart').subscribe((event) => {
        this.msjError(event);
      });

      this.inAppBrowserRef.on('exit').subscribe((event) => {
        // this.router.navigateByUrl('/tabs/cart');
        console.log('****** redirecionando por caso exit.');
        this.router.navigateByUrl('/tabs/home');
      });

    } else {
      // modo Web local

      // abrir url en nueva pestaña
      const newTab = window.open(this.urlPay.launch, '_blank');
      newTab.focus();


      this.timeControl = setInterval(() => {

        this.apiCall.payCheck().subscribe(resp => {
          if (resp.ok) {
            // caso de respuesta

            switch (resp.response) {
              case 'OK':
                this.webPayOK();
                break;

              case 'KO':
                this.webPayKO();
                break;

              case 'WAIT':
                this.webPayWAIT();
                break;

              default:
                this.webPayERROR();
                break;
            }


          } else {
            // caso que el server mande error por algun motivo
            console.warn('El server respondió con ok=FALSE al endpoint "/rest/pay/check"');
          }
        })

      }, this.loopTimeCheck);



    }
  }

  ionViewWillLeave() {
    // En caso que este activo el loop de tiempo se elimina antes de salir
    if (this.timeControl) { clearInterval(this.timeControl); }
  }


  msjError(event: InAppBrowserEvent) {
    console.log('calló en el evento de verificar url');

    if (event.url.includes(this.urlPay.OK)) {
      console.log('****** Entró en url igual para caso OK');
      console.log('****** urlObtenida:', event.url);
      this.inAppBrowserRef.close();
      this.myV.carritoActualizar(0);
      this.myF.showAlertOk('Notificación', 'done', 'Excelente..</br> Compra Notificada Exitosamente..');
      console.log('****** redireccionando la app');
      this.router.navigateByUrl('/tabs/home');
      console.log('****** redireccionado a TABS/HOME');
    } else if (event.url.includes(this.urlPay.KO)) {
      console.log('****** Entró en url igual para caso KO');
      console.log('****** urlObtenida:', event.url);
      this.inAppBrowserRef.close();
      this.myF.showAlertOk('Notificación', 'info', 'Compra Cancelada..</br> Estamos a la orden.. </br> Intente nuevamente..');
      console.log('****** redireccionando la app');
      this.router.navigateByUrl('/tabs/home');
      console.log('****** redireccionado a TABS/HOME');
    } else {
      console.log('****** no contiene url deseadas');
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  /// FUNCIONES PARA CASO WEB
  ///////////////////////////////////////////////////////////////////////////////////////////


  btnVolver() {
    console.log('BtnVolver presionado');
    clearInterval(this.timeControl);
    this.apiCall.payCheck().subscribe(resp => {
      if (resp.ok) {
        // caso de respuesta

        switch (resp.response) {
          case 'OK':
            this.webPayOK();
            break;

          case 'KO':
            this.webPayKO();
            break;

          case 'WAIT':
            // en Este caso si es "wait" se dirige a STOP por el usuario
            this.webPaySTOP();
            break;

          default:
            this.webPayERROR();
            break;
        }


      } else {
        // caso que el server mande error por algun motivo
        console.warn('El server respondió con ok=FALSE al endpoint "/rest/pay/check"');
      }
    })

  }


  /**
   * Caso cuanso se confirma el pago del usuario
   */
  webPayOK() {
    this.load = false;
    console.log('webPayOK');
    this.myV.carritoActualizar(0);
    if (this.timeControl) { clearInterval(this.timeControl); }
    this.myV.timeCallCamarero = moment().unix();
    this.myF.showAlertOk('¡Pago Exitoso!', 'done', 'Gracias por su compra..', '/tabs/home');

  }

  /**
   * Caso cuando ya se confirmo que el usuarió no pagó o falló el proceso de pago
   */
  webPayKO() {
    this.load = false;
    console.log('webPayKO');
    if (this.timeControl) { clearInterval(this.timeControl); }
    this.myV.timeCallCamarero = moment().unix();
    this.myF.showAlertOk('Pago no procesado', 'info',
      'El pago no pudo ser procesado o completado, </br> Inténtalo nuevamente..', '/tabs/cart');

  }

  /**
   * Caso cuando hay que seguir esperando
   */
  webPayWAIT() {
    console.log('En modo Espera, no se ha reportado ni pago ni errores');
  }

  /**
   * caso para falta de cabeceras o datos en la petición
   */
  webPayERROR() {
    this.load = false;
    console.log('webPayERROR');
    if (this.timeControl) { clearInterval(this.timeControl); }
    this.myV.timeCallCamarero = null;
    this.myF.showAlertOk('Inconvenientes en el pago', 'info',
      'Se presentaron inconvenientes en el pago, </br> Intente nuevamente..', '/');
  }

  /**
   * Caso cuando el usuario presiona el boton y no hay respuesta OK ni KO (regresa al carrito)
   */
  webPaySTOP() {
    this.load = false;
    console.log('webPaySTOP');
    if (this.timeControl) { clearInterval(this.timeControl); }
    this.myV.timeCallCamarero = moment().unix();
    this.myF.showAlertOk('Pago no procesado', 'info',
      'El pago no pudo ser procesado o completado, </br> Inténtalo nuevamente..', '/tabs/cart');

  }






}
