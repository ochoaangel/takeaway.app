import { Component, OnInit } from '@angular/core';
import { ModalController, IonRouterOutlet } from '@ionic/angular';
import { ExtraComponent } from "./extra/extra.component";
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';

@Component({
  selector: 'app-restaurant-menu',
  templateUrl: './restaurant-menu.page.html',
  styleUrls: ['./restaurant-menu.page.scss'],
})
export class RestaurantMenuPage implements OnInit {
  items = [
    {
      id: 1,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: true
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: false,
      discount: true
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: true,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: true,
      promoted: false,
      discount: false
    }, {
      id: 2,
      ratings: "5.0",
      route: "/restaurant-menu",
      img: "https://via.placeholder.com/200x200",
      name: "Title Text",
      tag: "tag1, tag2, tag3,tag4",
      delivery: "35 min",
      tracking: false,
      promoted: false,
      discount: false
    }
  ];

  categoria;
  productos;

  constructor(
    private routerOutlet: IonRouterOutlet,
    private modalCtrl: ModalController,
    private apiCall: ApiCallService,
    private myVar: MyVarService,
  ) { }

  ngOnInit() {

    this.categoria = this.myVar.initRestaurantMenu;
    console.log(' this.categoria', this.categoria);

    this.productos = this.items
    // this.apiCall.getProduct({ id: this.categoria.id + '' }).subscribe(resProductos => {
    //   if (resProductos.ok) {
    //     this.productos = resProductos.payload;
    //     console.log('this.productos', this.productos);
    //   } else {
    //     console.log('error al obtener productos');

    //   }
    // });


  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ExtraComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
  }

}
