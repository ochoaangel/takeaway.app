import { Component, OnInit } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Router } from '@angular/router';
import { InitConfigService } from 'src/app/services/init-config.service';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { MyVarService } from 'src/app/services/my-var.service';

@Component({
  selector: 'app-no-signal',
  templateUrl: './no-signal.page.html',
  styleUrls: ['./no-signal.page.scss'],
})
export class NoSignalPage implements OnInit {

  loading = false;
  constructor(
    private router: Router,
    private myF: MyFunctionsService,
    private myV: MyVarService,
    private apiCall: ApiCallService,
    private initconfig: InitConfigService,
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.myV.avoidSplash = false;
  }

  volver() {
    setTimeout(() => {
      this.loading = false;
    }, 1500);
    this.loading = true;
    this.apiCall.config({ test: 'test' }).subscribe(apiConfig => {
      this.loading = false;
      console.log('apiC', apiConfig);
      if (apiConfig.ok) {
        this.myV.avoidSplash = false;
        this.initconfig.initializeApp();
      }
    });
  }

}
