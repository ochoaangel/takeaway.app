import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoSignalPageRoutingModule } from './no-signal-routing.module';

import { NoSignalPage } from './no-signal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoSignalPageRoutingModule
  ],
  declarations: [NoSignalPage]
})
export class NoSignalPageModule {}
