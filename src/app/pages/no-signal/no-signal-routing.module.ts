import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoSignalPage } from './no-signal.page';

const routes: Routes = [
  {
    path: '',
    component: NoSignalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoSignalPageRoutingModule {}
