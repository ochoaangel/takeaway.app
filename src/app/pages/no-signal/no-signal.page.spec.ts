import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoSignalPage } from './no-signal.page';

describe('NoSignalPage', () => {
  let component: NoSignalPage;
  let fixture: ComponentFixture<NoSignalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoSignalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoSignalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
