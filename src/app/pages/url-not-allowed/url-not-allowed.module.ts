import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UrlNotAllowedPageRoutingModule } from './url-not-allowed-routing.module';

import { UrlNotAllowedPage } from './url-not-allowed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UrlNotAllowedPageRoutingModule
  ],
  declarations: [UrlNotAllowedPage]
})
export class UrlNotAllowedPageModule {}
