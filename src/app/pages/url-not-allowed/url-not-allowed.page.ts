import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-url-not-allowed',
  templateUrl: './url-not-allowed.page.html',
  styleUrls: ['./url-not-allowed.page.scss'],
})
export class UrlNotAllowedPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
