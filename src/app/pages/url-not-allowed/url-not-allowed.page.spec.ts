import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UrlNotAllowedPage } from './url-not-allowed.page';

describe('UrlNotAllowedPage', () => {
  let component: UrlNotAllowedPage;
  let fixture: ComponentFixture<UrlNotAllowedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlNotAllowedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UrlNotAllowedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
