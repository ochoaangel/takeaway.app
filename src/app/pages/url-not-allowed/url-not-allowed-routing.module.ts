import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UrlNotAllowedPage } from './url-not-allowed.page';

const routes: Routes = [
  {
    path: '',
    component: UrlNotAllowedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UrlNotAllowedPageRoutingModule {}
