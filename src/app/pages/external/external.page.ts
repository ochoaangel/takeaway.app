import { Component, OnInit } from '@angular/core';
import { MyVarService } from 'src/app/services/my-var.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions, InAppBrowserEvent } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-external',
  templateUrl: './external.page.html',
  styleUrls: ['./external.page.scss'],
})
export class ExternalPage implements OnInit {


  urlExternal;
  inAppBrowserRef;

  options: InAppBrowserOptions = {
    location: 'no', //Or 'no'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    beforeload: 'yes',
  };


  constructor(
    private myV: MyVarService,
    private apiCall: ApiCallService,
    private router: Router,
    private iab: InAppBrowser
  ) {
  }


  ngOnInit() {

    this.urlExternal = this.myV.initExternal;
    // this.inAppBrowserRef = this.iab.create(this.urlExternal, '_self');
    this.inAppBrowserRef = this.iab.create(this.urlExternal, '_blank');

    this.inAppBrowserRef.on('exit').subscribe((event) => {
      this.router.navigateByUrl('/tabs/home');
    });

  }


}
