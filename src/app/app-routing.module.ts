import { InitGuard } from "./guards/init-Guard/init-guard.guard";
import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "splash",
    pathMatch: "full",
  },
  {
    path: "splash",
    loadChildren: () =>
      import("./splash/splash.module").then((m) => m.SplashPageModule),
  },
  {
    path: "tabs",
    loadChildren: () =>
      import("./pages/tabs/tabs.module").then((m) => m.TabsPageModule),
  },
  {
    path: "restaurant-menu",
    loadChildren: () =>
      import("./pages/restaurant-menu/restaurant-menu.module").then(
        (m) => m.RestaurantMenuPageModule
      ),
  },
  {
    path: "tracking",
    loadChildren: () =>
      import("./pages/tracking/tracking.module").then(
        (m) => m.TrackingPageModule
      ),
  },
  {
    path: "authentication",
    loadChildren: () =>
      import("./authentication/authentication.module").then(
        (m) => m.AuthenticationPageModule
      ),
  },
  {
    path: "no-signal",
    loadChildren: () =>
      import("./pages/no-signal/no-signal.module").then(
        (m) => m.NoSignalPageModule
      ),
  },
  {
    path: "payment",
    loadChildren: () =>
      import("./pages/payment/payment.module").then((m) => m.PaymentPageModule),
  },
  {
    path: "external",
    loadChildren: () =>
      import("./pages/external/external.module").then(
        (m) => m.ExternalPageModule
      ),
  },
  {
    path: "init-with-children",
    loadChildren: () =>
      import("./pages/init-with-children/init-with-children.module").then(
        (m) => m.InitWithChildrenPageModule
      ),
  },
  {
    path: "init-into-local",
    loadChildren: () =>
      import("./pages/init-into-local/init-into-local.module").then(
        (m) => m.InitIntoLocalPageModule
      ),
  },
  {
    path: "url-not-allowed",
    loadChildren: () =>
      import("./pages/url-not-allowed/url-not-allowed.module").then(
        (m) => m.UrlNotAllowedPageModule
      ),
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
