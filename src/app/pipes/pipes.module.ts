import { NgModule } from '@angular/core';
import { PrecioPipe } from './precio.pipe';
import { MydatesPipe } from './mydates.pipe';

@NgModule({
  declarations: [PrecioPipe, MydatesPipe],
  imports: [],
  exports: [PrecioPipe,MydatesPipe]
})
export class PipesModule { }
