import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'mydates'
})
export class MydatesPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    let end = moment(value, 'YYYY-MM-DD HH:mm:ss').format(args[0]);
    return end;
  }

}
