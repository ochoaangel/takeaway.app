export interface User {
    id: number;
    uid: string;
    nombre: string;
    email: string;
}