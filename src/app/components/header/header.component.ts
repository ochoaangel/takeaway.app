import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { NavController } from "@ionic/angular";
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { MyStorageService } from "src/app/services/my-storage.service";
import { MyVarService } from "src/app/services/my-var.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  child: any = [];
  showInfo = true;
  constructor(
    public myV: MyVarService,
    public myF: MyFunctionsService,
    private navCtrl: NavController,
    private router: Router,
  ) { }


  ngOnInit() {

    let url = this.router.url
    console.log('url: ',url);
    if (url.includes('init-with-children')) {
      this.showInfo=false;
      console.log('showInfo',this.showInfo);
    }
   }
  ionViewWillEnter(){
  }

  getChild() {
    return JSON.parse(localStorage.getItem("childSelected"));
  }

  public goBack() {
    this.myF.actualizarUid()
    localStorage.removeItem("appZoneSelected");
    this.navCtrl.pop();
  }
}
