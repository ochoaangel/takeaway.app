import { MyVarService } from "src/app/services/my-var.service";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Injectable } from "@angular/core";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";
import { ToastController } from "@ionic/angular";
import {
  NativeGeocoderOptions,
  NativeGeocoderResult,
  NativeGeocoder,
} from "@ionic-native/native-geocoder/ngx";
@Injectable({
  providedIn: "root",
})
export class GeolocationService {
  subscription: any;
  locationCoords: any;
  apiResponse: any;
  postalCode: string;
  address: { code: string; nombre: string; ciudad: string };
  constructor(
    private diagnostic: Diagnostic,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private toastCtrl: ToastController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private myV: MyVarService
  ) {
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: "",
    };
  }
  //To check whether Location Service is enabled or Not
  async locationStatus() {
    return new Promise((resolve, reject) => {
      this.diagnostic
        .isLocationEnabled()
        .then((isEnabled) => {
          console.log("33", isEnabled);
          if (isEnabled === false) {
            resolve(false);
          } else if (isEnabled === true) {
            resolve(true);
          }
        })
        .catch((e) => {
          this.displayToast("Por favor encienda la ubicación");
          reject(false);
        });
    });
  }
  async checkLocationEnabled() {
    return new Promise((resolve, reject) => {
      this.diagnostic
        .isLocationEnabled()
        .then(async (isEnabled) => {
          console.log("51", isEnabled);
          if (isEnabled === false) {
            //this.showToast("Por favor encienda la ubicación Service");
            this.displayToast("Por favor encienda la ubicación");
            await this.requestGPSPermission();
            resolve(false);
          } else if (isEnabled === true) {
            this.checkGPSPermission()
              .then(async (response) => {
                console.log(
                  response,
                  "checkGPSPermission-checkLocationEnabled"
                );
                this.apiResponse = response;
                if (this.apiResponse === false) {
                  reject(false);
                  console.log("Permisos no aceptados");
                } else {
                  resolve(this.apiResponse);
                  console.log("Todos los permisos aceptados");
                  let result = await this.geolocation.getCurrentPosition();
                  this.geocoder(
                    result.coords.latitude,
                    result.coords.longitude
                  );
                }
              })
              .catch((e) => {
                console.log(e, "checkGPSPermission-checkLocationEnabled");
                reject(false);
              });
          }
        })
        .catch((e) => {
          //this.showToast("Por favor encienda la ubicación");
          this.displayToast("Por favor encienda la ubicación");

          reject(false);
        });
    });
  }
  //Check if application having GPS access permission
  async checkGPSPermission() {
    return new Promise((resolve, reject) => {
      this.androidPermissions
        .checkPermission(
          this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
        )
        .then(
          (result) => {
            console.log("93", result.hasPermission);
            if (result.hasPermission) {
              console.log("hasPermission-YES");
              //If having permission show 'Turn On GPS' dialogue
              this.askToTurnOnGPS().then((response) => {
                console.log(response, "askToTurnOnGPS-checkGPSPermission");
                if (this.apiResponse === false) {
                  reject(this.apiResponse);
                } else {
                  resolve(this.apiResponse);
                }
              });
            } else {
              console.log("hasPermission-NO");
              //If not having permission ask for permission
              this.requestGPSPermission().then((response) => {
                console.log(
                  response,
                  "requestGPSPermission-checkGPSPermission"
                );
                this.apiResponse = response;
                if (this.apiResponse === false) {
                  reject(this.apiResponse);
                } else {
                  resolve(this.apiResponse);
                }
              });
            }
          },
          (err) => {
            alert(err);
            reject(false);
          }
        );
    });
  }
  async requestGPSPermission() {
    return new Promise((resolve, reject) => {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          this.androidPermissions
            .requestPermission(
              this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
            )
            .then(
              () => {
                // call method to turn on GPS
                this.askToTurnOnGPS().then((response) => {
                  console.log(response, "askToTurnOnGPS-requestGPSPermission");
                  this.apiResponse = response;
                  if (this.apiResponse === false) {
                    console.log("148");

                    reject(this.apiResponse);
                  } else {
                    console.log("152");

                    resolve(this.apiResponse);
                  }
                });
              },
              (error) => {
                //Show alert if user click on 'No Thanks'
                console.log("159");
                reject(false);
              }
            );
        } else {
          //Show 'GPS Permission Request' dialogue
          this.androidPermissions
            .requestPermission(
              this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
            )
            .then(
              () => {
                // call method to turn on GPS
                this.askToTurnOnGPS().then((response) => {
                  console.log(response, "askToTurnOnGPS-requestGPSPermission");
                  this.apiResponse = response;
                  if (this.apiResponse === false) {
                    console.log("179");

                    reject(this.apiResponse);
                  } else {
                    console.log("183");

                    resolve(this.apiResponse);
                  }
                });
              },
              (error) => {
                //Show alert if user click on 'No Thanks'
                console.log("190");
                reject(false);
              }
            );
        }
      });
    });
  }
  async askToTurnOnGPS() {
    return new Promise((resolve, reject) => {
      this.locationAccuracy
        .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
        .then(
          (resp) => {
            console.log(resp, "location accuracy");
            // When GPS Turned ON call method to get Accurate location coordinates
            if (resp["code"] === 0) {
              resolve(this.apiResponse);
            }
            (error) => {
              reject(false);
            };
          },
          (err) => {
            console.log("228", err);
            this.displayToast(
              "Disculpe debe activar el servicio de ubicación para poder hacer uso de TakeWay.App"
            );
            this.askToTurnOnGPS();
          }
        );
    });
  }

  async displayToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }

  geocoder(lat, lng) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };

    this.nativeGeocoder
      .reverseGeocode(lat, lng, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log("***Direccion actual del dispositivo", result[0]);
        this.postalCode = result[0].postalCode;
        this.myV.postalCode = this.postalCode;
        this.address = {
          code: result[0].postalCode,
          nombre: result[0].subAdministrativeArea,
          ciudad: result[0].locality,
        };
      })
      .catch((error: any) => console.log(error));
  }

  comparePostalCode(zones) {
    return new Promise((resolve, reject) => {
      let index = zones.map((zone) => zone.code).indexOf(this.postalCode);
      if (index >= 0) {
        resolve(zones[index]);
      } else {
        reject(this.address);
      }
    });
  }
}
