import { User } from './../interfaces/interfaces';
import { Subject } from 'rxjs/internal/Subject';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MyVarService {
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
    // charlie
  //  uidDefault = 'TR52016336GK25ADK21GS934F5';

   //gran hermano
  uidDefault = 'TR23304797T53H9P8C2HK545H3';
  FirebaseSenderID = '679394108708';
  OneSignalAppID = 'b9a9df94-c634-493e-bcf8-8b2a09e861d5';

  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  uid = '';
  uidAux = '';                // caso cuando hay hijos y padres y en caso de local

  // OneSignalUID del Cliente (hijo) generado por Onesignal (padre)
  DvDeviceUid = '';

  // id del carrito
  dvcartUID = '';
  user: User;
  postalCode = '';
  urlBaseImg = 'https://backend.takeaway.cool';

  ////////////////////////////////////////////////////////////////////////////
  ///////////// variables iniciales para arrancar páginas  ///////////////////
  initRestaurantMenu;
  initShowProduct;            // idProduct
  initReadMore;               // idProduct
  initAddToCart;              // idProduct
  initExternal;               // url
  initPayment;                // {launch:"..",  KO:"..", OK:".."}
  initMenuCategoryId = -1;    // id de categoria referenciado desde el home
  cantidadCarrito = 0;        // cantidad de productos en el carrito

  ////////////////////////////////////////////////////////////////////////////
  ///////////// varibles utiles generales
  appInfo;
  products;
  categories;
  avoidSplash = false;  // usado cuando inicia en false y luego del splash se pone a true(no muestra el splash al retroceder)
  searchText = '';      // texto a buscar en Home y carta


  willEnterFirstTimeSplash = true;  // indica si es la primera vez vá a iniciar splash (inicialmente está en true para entrar al splash por primera vez, despues se pondrá en false)
  nowCordova;                       // se coloca desde el appComponent, booleano para definir si es cordova o web


  // solo tendrá valor (unixTime del momento en que ingresa con qr de una mesa) cuando alguien compra dentro del local
  intoLocal;                    // unix Time => dentro del local(autoServicio y llamarCamarero)
  timeOnTable = 3 * 60 * 60;    // segundos disponibles para comprar en local en una mesa
  mesaCode;                     // codigo de la mesa
  tiendaLocalUid;               // uid dado por url unicamente en modo web en el inicio
  intoLocalMode;                //  'llamarCamarero' || 'autoServicio'

  kioscoCode;

  timeCallCamarero;

  haveChildren = false;            //  true=> ya se ha iniciado la config con TKWREST default    // false=> NO se ha iniciado la config con TKWREST default
  tkwRestAuxUid: string = null;   // cuando tiene valor, inicializaría la app con este tkwrest, no por el default..

  ////////////////////////////////////////////////////////////////////////////
  //////////// variable dinamica
  public carritoDinamico = new Subject<any>();

  constructor() { }

  // trabaja en conjunto con variable carrito Dinamico
  carritoActualizar(val) {
    this.carritoDinamico.next(val);
  }


}
