import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MyVarService } from './my-var.service';

@Injectable({
  providedIn: 'root',
})
export class ApiCallService {
  baseUrl;

  constructor(private http: HttpClient, private myV: MyVarService) {
    // this.baseUrl = 'https://takeaway-cool-app-backend.herokuapp.com/api/v1.0';
    // this.baseUrl = 'http://localhost:3000/api/v1.0';
    this.baseUrl = 'https://backend.takeaway.cool/api/v1/rest';
  }

  ////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  GET  ///////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  isOperative(): Observable<any> {
    const url = '/is-operative';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }
  getHome(): Observable<any> {
    let tkwRestUid;
    if (this.myV.uid) {
      tkwRestUid = this.myV.uid;
    } else {
      tkwRestUid = 'XXXXXXXXXXXXXXX_error_XXXXXXXXXXXXXXX';
      console.error('---ERROR, falta TKW-REST-UID para no tener ERROR---');
    }
    const url = `/home?TKW-REST-UID=${tkwRestUid}`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }

  getNewHome(): Observable<any> {
    let tkwRestUid;
    if (this.myV.uid) {
      tkwRestUid = this.myV.uid;
    } else {
      tkwRestUid = 'XXXXXXXXXXXXXXX_error_XXXXXXXXXXXXXXX';
      console.error('---ERROR, falta TKW-REST-UID para no tener ERROR---');
    }
    const url = `/home?TKW-REST-UID=${tkwRestUid}`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }

  config(params): Observable<any> {
    const url = '/config';
    const urlFinal = this.baseUrl + url;
    if (params.config_uid) {
      return this.http.get<any>(urlFinal, { params });
    } else {
      return this.http.get<any>(urlFinal, {});
    }
  }

  productById(idProduct): Observable<any> {
    const url = '/products/' + idProduct;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  productByCat(): Observable<any> {
    const url = '/categories/1/products';
    const urlFinal = '../../assets/apiJson/productByCat.json';
    return this.http.get<any>(urlFinal, {});
  }

  categories(params?): Observable<any> {
    const url = '/categories';
    const urlFinal = this.baseUrl + url;
    if (params) {
      return this.http.get<any>(urlFinal, { params });
    } else {
      return this.http.get<any>(urlFinal, {});
    }
  }

  cart(): Observable<any> {
    const url = '/cart';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  cartCount(): Observable<any> {
    const url = '/cart?asCount=1';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  getChildren(): Observable<any> {
    const url = `/home/rest-branches?TKW-REST-UID=${this.myV.uid}&code=${this.myV.postalCode}`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  getZones(): Observable<any> {
    const url = `/branches/scope?TKW-REST-UID=${this.myV.uid}`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// POST ////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////

  payCheck(): Observable<any> {
    const url = `/pay/check`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  cartAdd(params): Observable<any> {
    const url = '/cart/add';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  payWith(params): Observable<any> {
    const url = `/pay/with/${params.id}${params.vuelta ? `?ref=${params.vuelta}€` : ''
      }`;
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal, {});
  }

  clienRegisterAnom(params): Observable<any> {
    const url = '/clients/register/anom';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  clienRegister(params): Observable<any> {
    const url = '/clients/register';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  clienLogin(params): Observable<any> {
    const url = '/clients/login';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  clientOrders(): Observable<any> {
    const url = '/clients/my-orders';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, {});
  }

  cartRem(params): Observable<any> {
    const url = '/cart/rem';
    const urlFinal = this.baseUrl + url;
    if (params) {
      return this.http.post<any>(urlFinal, {}, { params });
    } else {
      return this.http.post<any>(urlFinal, {}, {});
    }
  }

  cartSyncDetails(params): Observable<any> {
    const url = '/cart/sync-details';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  clients(): Observable<any> {
    const url =
      '/cart/add?nombre=Victor Maduro&email=victordmadurom@gmail.com&tel=604314426';
    const urlFinal = '../../assets/apiJson/clients.json';
    return this.http.post<any>(urlFinal, {});
  }

  aviableZones(): Observable<any> {
    const url = '/available-zones';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }

  myZones(): Observable<any> {
    const url = '/clients/my-zones';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, {});
  }

  forgotPassword(params): Observable<any> {
    const url = '/clients/forgot-password';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  aviablePaymentMethods(): Observable<any> {
    const url = '/pay/available-methods';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }

  callService(): Observable<any> {
    const url = '/call-service';
    const urlFinal = this.baseUrl + url;
    return this.http.get<any>(urlFinal);
  }

  CartPlacesGet(): Observable<any> {
    const url = '/clients/places';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, {});
  }

  /**
   *   { tkwrestzone_id: number, dir: string, dir2: number, default: boolean }
   * @param params  { tkwrestzone_id: number, dir: string, dir2: number, default: boolean }
   */
  CartPlacesAdd(params): Observable<any> {
    const url = '/clients/places/add';
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  /**
   *  mandar ID ejemplo: 10
   * @param id number
   */
  CartPlacesSetDefault(id): Observable<any> {
    const url = `/clients/places/set-default/${id}`;
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, {});
  }

  /**
   *  mandar ID ejemplo: 10 
   * @param id number
   */
  CartPlacesDelete(id): Observable<any> {
    const url = `/clients/places/rem/${id}`;
    const urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, {});
  }


}
