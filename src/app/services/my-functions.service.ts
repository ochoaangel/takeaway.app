import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MyVarService } from './my-var.service';
import { ApiCallService } from './api-call.service';
import { AlertController, Platform } from '@ionic/angular';
import { MyStorageService } from './my-storage.service';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})

export class MyFunctionsService {


  constructor(
    public myV: MyVarService,
    public myS: MyStorageService,
    public router: Router,
    private apiCall: ApiCallService,
    public alertCtrl: AlertController,
    public platform: Platform,

  ) { }


  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////// Compuestas  ////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * actualizo en myV los productos y categorías
   */
  updateProductsAndCategories() {

    this.apiCall.categories({ include: 'products' }).subscribe((cat) => {

      // preparo todos los productos a mi manera
      this.myV.products = this.prepareProducts(cat.data);

      // preparo solo categorias
      this.myV.categories = this.getUnique(this.myV.products, 'catId');
      console.log('Actualizado (Products y Categories) en myV.');
      console.log('this.myV.products', this.myV.products);
      // console.log('this.myV.categories', this.myV.categories);

    });

  }


  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////// INDIVIDUALES ////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @param fromApi  'resultado de api-> /categories'
   *
   * crea un array con toda la info organizada de los productos con categoria y todo
   *
   */
  prepareProducts(fromApi) {
    const end = [];
    fromApi.forEach(cat => {
      cat.products.forEach(prod => {
        end.push({
          // categoria
          catIcon: cat.icon,
          catId: cat.id,
          catNombre: cat.nombre,
          catDescription: cat.descripcion || 'Descipción..',

          // producto
          prodDescuentos: prod.descuentos,
          prodId: prod.id,
          prodImagen: prod.imagen,
          prodImagen_min: prod.imagen_min,
          prodImagen_sqr: prod.imagen_sqr,
          prodNombre: prod.nombre,
          prodPrecio: prod.precio,
          prodDescription: prod.descripcion,
          prodPromotion: prod.promotion
        });
      });
    });
    return end;
  }


  fontsAdd(myfonts: string[]) {
    if (myfonts && myfonts.length > 0) {

      let count = 0;
      myfonts.forEach(font => {

        count++;

        let fontFamily = font;
        const fontUri = `https://fonts.googleapis.com/css?family=${fontFamily.replace(/ /g, '+')}`;
        const pos = fontFamily.indexOf(':');
        if (pos > 0) {
          fontFamily = fontFamily.substring(0, pos);
        }
        let link = document.getElementById('myfontLink' + count);
        if (!link) {
          link = document.createElement('link');
          link.id = 'myfontLink' + count;
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', fontUri);
          document.head.appendChild(link);
        } else {
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', fontUri);
        }

      });
    } else {
      console.warn('myError, no se recibiernon fuentes en funcion fontsAdd');
    }


  } // fin fontsAdd


  /**
   *
   * @param collection coleccion a donde se desea buscar un solo objeto para cada keyname
   * @param keyName key especifico en el cual se elegirá el primero que no se repita
   *
   * dada una colección, digamos que se clasifica por un keyname y elige solo el primer
   *  objeto con ese keyname
   *
   */
  getUnique(collection, keyName) {

    // store the comparison  values in array
    const unique = collection.map(e => e[keyName])

      // store the indexes of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the false indexes & return unique objects
      .filter((e) => collection[e]).map(e => collection[e]);

    return unique;
  }



  searchingText(textoAbuscar) {
    let end = [];
    let arrToSeach = [];
    const text = textoAbuscar.toLowerCase().replace(/,/g, ' ').replace(/\./g, ' ');
    const products = this.myV.products;

    // separando por espacios y con valores
    arrToSeach = text.split(' ').filter(Boolean);

    console.log('arrToSeach', arrToSeach);

    // Guardo en end el ersultado del iltro
    end = products.filter(producto => {
      let pasa = false;     // por defecto

      // si encuentro algo lo pongo a true
      arrToSeach.forEach(palabra => {
        if (producto.prodNombre.toLowerCase().includes(palabra)) {
          pasa = true;
        }
      });

      // mando resultado
      return pasa;
    });

    return end;
  }


  // alerts
  /** */
  async showAlertOk(titulo: string, icon: string, mensajeHTML: string, redirectTo?: string) {

    let icono;
    switch (icon) {
      case 'info':
      case 'inf':
        icono = 'information-circle-outline';
        break;

      case 'done':
        icono = 'checkmark-done-circle-outline';
        break;

      default:
        icono = icon;
        break;
    }


    const alert = await this.alertCtrl.create({
      header: titulo,
      mode: 'ios',
      message: `</br><ion-icon name="${icono}"></ion-icon><br>${mensajeHTML}`,
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          handler: (blah) => {
            console.log('Confirm OK: blah');
            if (redirectTo) {
              this.router.navigateByUrl(redirectTo);
            }
          }
        }
      ]
    });
    await alert.present();
  }


  modeLocal() {
    let end = false;

    if (this.myV.intoLocal) {

      const initTime = this.myV.intoLocal;
      const nowTime = moment.utc().unix();
      // console.log(nowTime, initTime, nowTime - initTime, this.myV.timeOnTable);

      if ((nowTime - initTime) > this.myV.timeOnTable) {
        console.log('Borrar Mesa');
        this.myV.intoLocal = null;
        this.myS.remove('appClienteIntoLocalTime').subscribe(z => console.log('Borrado tiempo de Mesa->', z));
        end = false;
      } else {
        // Caso de Tiempo vigente en la mesa dentro del local
        end = true;
      }

    } else {
      end = false;
    }

    return end;
  }


  /**
   * Prepara el uid de la app, si es hijo, padre, con mesa, etc,
   * Coloca el uidNuevo como principal y el por default en el auxiliar
   * en caso de no estar utilizando ninguno, define el por default
   * 
   * @param uidNuevo  string
   */
  actualizarUid(uidNuevo?: string) {
    if (uidNuevo && uidNuevo !== this.myV.uid && uidNuevo !== this.myV.uidAux) {
      this.myV.uid = uidNuevo;
      this.myV.uidAux = this.myV.uidDefault;
    } else {
      this.myV.uid = this.myV.uidDefault;
      this.myV.uidAux = null;
    }

  }

  /**
   * obtengo el uid de inicio
   */
  getUID(): string {
    let end: string;

    if (this.myV.uid) {
      end = this.myV.uid
    } else {
      end = this.myV.uidDefault
    }
    return end;
  }


  llamarCamareroAndNotificar() {

    if (!this.myV.timeCallCamarero) {
      // primera vez
      this.myV.timeCallCamarero = moment().unix();
      this.apiCall.callService().subscribe(resp => {
        if (resp.ok) {
          this.showAlertOk('¡Éxito!', 'done', 'Hemos notificado al Camarero..');

        } else {
          this.showAlertOk('Notificación', 'info', 'Hubo inconvenientes notificado al Camarero');

        }
      });

    } else {
      // caso que ya exite un tiempo o llamada previa al camarero
      let oldTime = this.myV.timeCallCamarero;
      let nowTime = moment().unix();

      if ((nowTime - oldTime) > 30) {
        this.apiCall.callService().subscribe(resp => {
          if (resp.ok) {
            this.showAlertOk('¡Éxito!', 'done', 'Hemos notificado al Camarero..');
            this.myV.timeCallCamarero = moment().unix();
          } else {
            this.showAlertOk('Notificación', 'info', 'Hubo inconvenientes notificado al Camarero');

          }
        });

      } else {
        // caso que aun no ha pasado el tiempo
        this.showAlertOk('Notificación', 'info', 'Ya fué notificado al Camarero..');

      }

    }
  }

}   //  fin clase ppal