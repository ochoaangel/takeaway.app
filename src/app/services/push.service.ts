import { Platform } from '@ionic/angular';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Injectable } from '@angular/core';
import { MyVarService } from './my-var.service';

@Injectable({
  providedIn: 'root'
})

export class PushService {


  constructor(
    private myV: MyVarService,
    private platform: Platform,
    private oneSignal: OneSignal,
  ) { }

  async configuracionInicial() {

    let startInit;

    if (
      this.platform.is('ios') ||
      this.platform.is('ipad') ||
      this.platform.is('iphone')
    ) {
      // caso IOS
      const iosSettings = { kOSSettingsKeyAutoPrompt: true, kOSSettingsKeyInAppLaunchURL: false };

      startInit = this.oneSignal
        .startInit(this.myV.OneSignalAppID, this.myV.FirebaseSenderID)
        .iOSSettings(iosSettings);
    } else {
      // caso Android
      startInit = this.oneSignal
        .startInit(this.myV.OneSignalAppID, this.myV.FirebaseSenderID);
    }

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // do something when notification is received
      console.log('Notificación recibida', noti);
    });

    this.oneSignal.handleNotificationOpened().subscribe(async (noti) => {
      // do something when a notification is opened
      console.log('Notificación abierta', noti);
    });

    this.oneSignal.endInit();

    try {
      console.log('ONESIGNAL >> Entrando a Try');
      const resp = await this.oneSignal.getIds();

      console.log('ONESIGNAL >> UID Recibido: ' + resp.userId);

      this.myV.DvDeviceUid = resp.userId || '';
      if (!this.myV.DvDeviceUid) {
        console.warn('ONESIGNAL >> No hay UID asignado');
      } else {
        console.log('ONESIGNAL UID ASIGNADO EN INICIO >> ' + this.myV.DvDeviceUid);
      }

    } catch (error) {
      console.error('ONESIGNAL >> Error al intentar getIds(): ' + JSON.stringify(error));
    }

  }

}
