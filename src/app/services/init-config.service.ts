import { Platform } from "@ionic/angular";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Injectable } from "@angular/core";
import { PushService } from "./push.service";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { MyVarService } from "./my-var.service";
import { ApiCallService } from "./api-call.service";
import { MyStorageService } from "./my-storage.service";
import { OneSignalService } from "./one-signal.service";
import { MyFunctionsService } from "./my-functions.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscriber } from "rxjs";

import * as _ from "underscore";
import * as moment from "moment";

@Injectable({
  providedIn: "root",
})
export class InitConfigService {
  constructor(
    private myF: MyFunctionsService,
    private myVar: MyVarService,
    private pushS: PushService,
    private router: Router,
    private platform: Platform,
    private myStorage: MyStorageService,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private nativeStorage: NativeStorage,
    private apiCallService: ApiCallService,
    private activatedRoute: ActivatedRoute,
    private oneSignalService: OneSignalService
  ) { }

  appInitConfig;

  //

  // funciones separadas

  /**
   *  coloca un tema
   * @param myTheme  =[ {themeVariable:'zzzzzz',value:'xxxxxx},[]..]
   */
  setTheme(myTheme) {
    const futureTheme = document.querySelector("body");
    myTheme.forEach((style) => {
      futureTheme.style.setProperty(style.themeVariable, style.value);
    });
  }

  /**
   * retorna un array con elementos { themeVariable: 'zzzzzz', value: 'xxxxxx }
   * @param string_theme_color_generated
   */
  prepareThemeFromColorGenerator(string_theme_color_generated: string) {
    // quitando posible ";" al final
    let textColor = string_theme_color_generated.trim();
    const lastChar = textColor.charAt(textColor.length - 1);
    textColor =
      lastChar === ";"
        ? textColor.substring(0, textColor.length - 1)
        : textColor;

    // separando en array
    const myColors = textColor.split(";");

    // procesando cada linea
    const final = myColors.map((element) => {
      const result = element.split(":");
      return { themeVariable: result[0].trim(), value: result[1].trim() };
    });

    return final;
  }

  prepareAll(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      //////////////////////////////////////////////////////////////////////////////////////////////////////
      // revisar si hay configuración en local storage "appInitConfig"

      this.myStorage.get("appInitConfig").subscribe((resp) => {
        if (resp) {
          // caso que SI tengo guardado algo en el storage
          console.log("caso que SI tengo guardado algo en el storage");

          let config_uid = resp.config_uid;
          this.apiCallService.config({ config_uid }).subscribe((apiConfig) => {
            if (apiConfig.data) {
              const preparado = this.prepareConfig(apiConfig);
              this.setConfig(preparado);
              observer.next(true);
              observer.complete();
            } else {
              this.setConfig(resp);
              observer.next(true);
              observer.complete();
            }
          });
        } else {
          // caso que NO tengo guardado nada en el storage
          console.log("caso que NO tengo guardado nada en el storage");
          this.apiCallService
            .config({ config_uid: "" })
            .subscribe((apiConfig) => {
              if (apiConfig.data) {
                const preparado = this.prepareConfig(apiConfig);
                this.setConfig(preparado);
                observer.next(true);
                observer.complete();
              } else {
                console.warn("ERROR: recibiendo configuración inicial");
                observer.next(false);
                observer.complete();
              }
            });
        }
      }); // fin this.myStorage.get('appInitConfig').subscribe(resp
    }); // fin this.myStorage.get('appInitConfig').subscribe(resp
  }

  prepareConfig(directFromApi) {
    console.log("directFromApi", directFromApi);

    let initConfig = directFromApi.data;

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // gestionando paleta de colores
    if (initConfig.config.colors) {
      // obtengo la paleta de color bien organizada así {themeVariable: "--ion-color-primary", value: "#f15a24"}
      const paletaOrganizada = this.prepareThemeFromColorGenerator(
        initConfig.config.colors
      );

      // implemento paleta de colores
      this.setTheme(paletaOrganizada);

      if (this.platform.is("cordova")) {
        // busco el color primario en hexadecimal
        const primaryColorValue = paletaOrganizada
          .filter((x) => x.themeVariable === "--ion-color-primary")
          .map((y) => y.value)[0];

        // agrego color al statusBar (arriba donde aparece la hora y señal de tlf)
        this.statusBar.backgroundColorByHexString(primaryColorValue);
        initConfig.statusBarColor = primaryColorValue;
      }
    } else {
      console.warn("ERROR, No se encontó la PALETA de colores");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // gestionando fuentes
    if (initConfig.config.fonts) {
      initConfig.config.fontsCollection = initConfig.config.fonts;

      // preparo array de fuentes
      const fontsArray = initConfig.config.fonts.map((res) => res.name);
      initConfig.config.fontsArray = fontsArray;

      // agrego las fuentes
      this.myF.fontsAdd(fontsArray);

      const fontObj = {};
      initConfig.config.fonts.forEach((element) => {
        fontObj[element.rol] = element.name;
      });
      initConfig.fonts = fontObj;

      // guardo en el localstorage
      this.myStorage
        .set("appInitConfig", initConfig)
        .subscribe((almacenado) => {
          if (almacenado) {
            console.log(
              "Exito: almacenada configuración inicial appInitConfig"
            );
          } else {
            console.warn(
              "ERROR: ---NO almacenada configuración inicial appInitConfig"
            );
          }
        });
    }
    return initConfig;
  }

  setConfig(old) {
    let initConfig = old;

    // almaceno todo en myVar
    this.myVar.appInfo = initConfig;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // gestiono paleta

    // obtengo la paleta de color bien organizada así {themeVariable: "--ion-color-primary", value: "#f15a24"}
    const paletaOrganizada = this.prepareThemeFromColorGenerator(
      initConfig.config.colors
    );

    // implemento paleta de colores
    this.setTheme(paletaOrganizada);

    // agrego color al statusBar (arriba donde aparece la hora y señal de tlf)
    this.statusBar.backgroundColorByHexString(initConfig.statusBarColor);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // gestiono fuente

    // agrego las fuentes
    this.myF.fontsAdd(initConfig.config.fontsArray);

    console.log("Exito: agregada configuración de inicio desde LocalStorage");
  }

  get(keyName): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      if (
        (this.platform.is("android") || this.platform.is("ios")) &&
        this.platform.is("cordova")
      ) {
        // caso dispositivo
        try {
          this.nativeStorage.getItem(keyName).then(
            (data) => {
              observer.next(data);
              observer.complete();
            },
            (err) => {
              observer.next();
              observer.complete();
            }
          );
        } catch (reason) {
          console.warn(reason);
          observer.next();
          observer.complete();
        }
      } else {
        // caso web
        observer.next(JSON.parse(localStorage.getItem(keyName)));
        observer.complete();
      }
    });
  } // fin get

  async initializeApp() {
    // this.splashScreen.hide();
    await this.platform.ready();

    if (this.platform.is("cordova")) {
      this.statusBar.backgroundColorByHexString("#000000");
      this.splashScreen.hide();

      // inicializa OneSignal en Cordova
      console.log("Iniciando configuración CON cordova");
      // dentro de cordova diferencia para caso Android y caso IOS en la siguiente función
      await this.pushS.configuracionInicial();
      // redirecciono a Splash para continuar a cargar el config
      this.router.navigateByUrl("/splash");
    } else {
      // inicializa OneSignal en Web
      console.log("Iniciando configuración SIN cordova");
      await this.oneSignalService.OneSignalWebInit();
      // redirecciono a Splash para continuar a cargar el config
      this.router.navigateByUrl("/splash");
    }
  }





}
