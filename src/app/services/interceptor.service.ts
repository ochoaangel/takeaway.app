import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { MyVarService } from './my-var.service';
import { catchError, tap } from 'rxjs/operators';
import { MyFunctionsService } from './my-functions.service';
import { Observable, throwError } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})

export class InterceptorService implements HttpInterceptor {

  constructor(
    private myV: MyVarService,
    private myF: MyFunctionsService,
    public router: Router,
    private platform: Platform,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let myData = { 'TKW-REST-UID': this.myF.getUID() };

    if (this.myV.uidAux) {
      myData['TKW-REST-AUX-UID'] = this.myV.uidAux;
    }

    // ID que asigna el ONESIGNAL al dispositivo
    if (this.myV.DvDeviceUid) {
      myData['DVDEVICE-UID'] = this.myV.DvDeviceUid;
    } else if (this.myV.nowCordova) {
      myData['DVDEVICE-UID'] = 'NO-DEVICE-UID';
    }

    // ID Usuario
    if (this.myV.user && this.myV.user.uid) {
      myData['DVCLIENT-UID'] = this.myV.user.uid;
    }

    if (!this.myV.nowCordova) {
      myData['mode'] = 'web';
    }

    if (this.myF.modeLocal()) {
      myData['local'] = 'true';
      myData['mesa'] = this.myV.mesaCode;
    }

    if (this.myV.dvcartUID) {
      myData['DVCART-UID'] = this.myV.dvcartUID;
    }

    const headers = new HttpHeaders(myData);
    const reqClone = req.clone({ headers });

    // caso de falla de internet redirecciona
    return next.handle(reqClone).pipe(
      tap({
        error: (res) => {
          this.myF.showAlertOk('Notificación', 'info',
            'Hay problemas de conexión..</br> Verifique la señal de internet e intente nuevamente..');

          console.warn('ERROR controlado,(falla de conexión)', res);
          this.router.navigateByUrl('no-signal');
        }
      }),
      catchError(this.manejarError)
    );
  }


  manejarError(error: HttpErrorResponse) {
    console.warn('Error gestionado en el Servicio Interceptor', error);
    return throwError(error);
  }



}
