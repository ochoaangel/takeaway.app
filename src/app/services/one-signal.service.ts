import { Injectable } from '@angular/core';
import { MyVarService } from './my-var.service';
let OneSignal;


@Injectable()
export class OneSignalService {

    constructor(private myV: MyVarService) { }


    async OneSignalWebInit() {
        OneSignal = window['OneSignal'] || [];
        const infoToOneSignal = ['init', {
            appId: this.myV.OneSignalAppID,
            autoRegister: true,
            allowLocalhostAsSecureOrigin: true,
            notifyButton: {
                enable: false
            }
        }];
        OneSignal.push(infoToOneSignal);

        OneSignal.push(() => {
            OneSignal.isPushNotificationsEnabled((isEnabled) => {
                if (isEnabled) {
                    OneSignal.getUserId().then((userId) => {
                        console.log('User ID is-------------------------------', userId);
                        this.myV.DvDeviceUid = userId;
                    });
                    console.log('>>>Push notifications are enabled!');

                } else {
                    console.log('>>>Push notifications are not enabled yet.');
                }
            }, (error) => {
                console.log('>>>>>Push permission not granted');
            });
        });




    }















}