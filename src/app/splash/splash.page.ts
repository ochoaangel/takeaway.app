import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MyVarService } from '../services/my-var.service';
import { ApiCallService } from '../services/api-call.service';
import { MyStorageService } from '../services/my-storage.service';
import { InitConfigService } from '../services/init-config.service';
import { Component, OnInit } from '@angular/core';
import { MyFunctionsService } from '../services/my-functions.service';
import { Platform } from '@ionic/angular';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {
  haveChildrenFromConfig;

  appIcon: HTMLLinkElement = document.querySelector('#appIcon');

  timeSplash;
  texto;

  classDivFondo;
  classDivLogo;
  classDivTexto;
  styleDivImageFondo;
  styleDivImageLogo;
  styleDivTexto;
  styleImageFondo;
  styleImageLogo;
  styleTexto;

  imgBackground;
  imgLogo;
  directFromApi;

  nowCordova;  // Definir si es cordova o no. booleano

  // timeSplash = { total: 1000, middle: 3000 };
  // texto = 'No olvides seguirnos!';
  // timeSplash = { total: 1000, middle: 3000 };
  // texto = 'No olvides seguirnos!';

  // classDivFondo = 'animated FadeIn Slower';
  // classDivLogo = 'animated flipInY';
  // classDivTexto = 'animated lightSpeedIn delay-1s';
  // styleDivImageFondo = {
  //   'z-index': '0',
  //   position: 'absolute',
  //   height: '100%',
  //   width: '100%',
  // };
  // styleDivImageLogo = {
  //   'z-index': '1',
  //   position: 'absolute',
  //   top: '20%',
  //   left: '8%',
  //   width: '85%',
  // };
  // styleDivTexto = {
  //   'z-index': '2',
  //   position: 'absolute',
  //   top: '70%',
  //   left: '28%',
  //   width: '80%',
  // };
  // styleImageFondo = {
  //   opacity: '1',
  //   width: '100%',
  //   height: '100%',
  //   'object-fit': 'fill',
  // };
  // styleImageLogo = { opacity: '1' };
  // styleTexto = {
  //   '-webkit-box-sizing': 'content-box',
  //   '-moz-box-sizing': 'content-box',
  //   'box-sizing': 'content-box',
  //   border: 'none',
  //   font: 'normal 1rem/1 Arial, Helvetica, sans-serif',
  //   color: 'orangered',
  //   '-o-text-overflow': 'ellipsis',
  //   'text-overflow': ' ellipsis',
  //   'text-shadow': '-4px -8px 46px rgba(38,153,38,1) ',
  // };

  constructor(
    public myV: MyVarService,
    public myS: MyStorageService,
    public myF: MyFunctionsService,
    private router: Router,
    public apiCall: ApiCallService,
    private platform: Platform,
    public initConfig: InitConfigService,
    private titleService: Title,
    private initConfigService: InitConfigService
  ) {
    this.myV.avoidSplash ? this.router.navigateByUrl('/tabs/home') : null;
  }

  ngOnInit() { }


  ionViewDidLeave() { console.warn('>>> SALIENDO DE SPLASH PARA NUNCA MAS VOLVER EN MODO FANTASMA jaja'); }

  ionViewWillEnter() {
    console.warn('>>> INICIANDO PÁGINA SPLASH');

    // caso para evitar vaer en el splash al presionar boton de retroceso dentro de la aplicación
    if (this.myV.avoidSplash) {
      this.router.navigateByUrl('/tabs/home');
    } else {

      this.apiCall.config({ config_uid: this.myF.getUID() }).subscribe((apiConfig) => {

        // Variable para saber si tiene hijo o no
        this.haveChildrenFromConfig = apiConfig.data.config.haveChildren ? apiConfig.data.config.haveChildren : false;
        this.myV.haveChildren = this.haveChildrenFromConfig;
        const preparado = this.initConfigService.prepareConfig(apiConfig);

        // obtengo datos de configuración
        this.initConfigService.setConfig(preparado);
        console.log('inicio de prepare all into Splash');


        let initConfig = this.myV.appInfo.config.splash;
        this.directFromApi = initConfig;

        this.classDivFondo = initConfig.css.classDivFondo;
        this.classDivLogo = initConfig.css.classDivLogo;
        this.classDivTexto = initConfig.css.classDivTexto;
        this.styleDivImageFondo = initConfig.css.styleDivImageFondo;
        this.styleDivImageLogo = initConfig.css.styleDivImageLogo;
        this.styleDivTexto = initConfig.css.styleDivTexto;
        this.styleImageFondo = initConfig.css.styleImageFondo;
        this.styleImageLogo = initConfig.css.styleImageLogo;
        this.styleTexto = initConfig.css.styleTexto;

        this.timeSplash = initConfig.timeSplash;
        this.texto = initConfig.texto;

        this.imgBackground = initConfig.images.Background;
        this.imgLogo = initConfig.images.Logo;

        // colocandole titulo a la pestaña Web General
        this.titleService.setTitle(this.myV.appInfo.nombre);

        //colocando icono a la WEB
        this.appIcon.href = this.myV.urlBaseImg + this.myV.appInfo.brand_logo;

        //////////////////////////////////////////////////////////////////////////////////
        // caso de animacion en medio del splash
        setTimeout(() => {
          // this.middleSplash();
          this.classDivLogo = this.directFromApi.css.classDivLogo;
          this.classDivTexto = this.directFromApi.css.classDivTexto;
          console.log('>>>> Terminado tiempo Medio del splash');
        }, this.timeSplash.middle);

        //////////////////////////////////////////////////////////////////////////////////
        // caso cuando termina el tiempo total
        setTimeout(() => {
          this.classDivLogo = 'animated fadeOut';

          // si hay usuario en el local storage lo copio en this.myV.user
          this.myS.get('user').subscribe((respUser) => {
            if (respUser) {
              this.myV.user = respUser;
              console.log('*****USUARIO LOGUEADO EN EL STORAGE');
            } else {
              console.warn('*****USUARIO NO LOGUEADO EN EL STORAGE');
            }

          });



          /////////////////////////////////////////////////////////////
          ////////////////// Seleccionando Destino ////////////////////
          /////////////////////////////////////////////////////////////
          const variablesNecesarias = {
            haveChildren: this.haveChildrenFromConfig,
          };
          console.log('variablesNecesarias', variablesNecesarias);
          this.seleccionandoElDestino(variablesNecesarias);
          /////////////////////////////////////////////////////////////

        }, this.timeSplash.total);
      });

    }
  }



  // seleccionando el camino
  seleccionandoElDestino(info) {

    if (this.myV.nowCordova) {
      // Caso Cordova
      this.destinoCordova(info);

    } else {
      // Caso WEB
      this.destinoWeb(info);
    }
  }


  destinoCordova(info) {

    if (info.haveChildren) {
      // caso con hijos
      this.initCordovaWithChildren(); console.log('initCordovaWithChildren()');
    } else {
      // caso sin hijos
      this.initCordova(this.myF.getUID());
    }


  }

  destinoWeb(info) {
    if (this.myV.mesaCode && this.myV.tiendaLocalUid) {
      this.myF.actualizarUid(this.myV.tiendaLocalUid);
      this.router.navigateByUrl('/init-into-local')
    } else {
      this.router.navigateByUrl('/url-not-allowed')
    }
  }



  initCordova(uidDeInicio) {
    console.log('initCordova() con uid: ' + uidDeInicio);
    this.router.navigateByUrl('/tabs/home');

  }

  initCordovaWithChildren() {
    this.router.navigateByUrl('/init-with-children');
  }


}
