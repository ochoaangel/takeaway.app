import { Component, OnInit } from '@angular/core';
import { MyVarService } from './services/my-var.service';
import { MyStorageService } from './services/my-storage.service';
import { InitConfigService } from './services/init-config.service';
import { MyFunctionsService } from './services/my-functions.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  // definición de parametros a leer de la url root
  parametroUrlMesa = 'tkwrestboard';
  parametroUrlKiosco = 'kiosco';
  parametroUrlTkwRestUid = 'tkwrestuid';

  plataformaDefault = 'cordova';   //   'cordova'||'web'|| null  (null es selección automática)

  constructor(
    private myV: MyVarService,
    private myS: MyStorageService,
    private myF: MyFunctionsService,
    private router: Router,
    private initConfig: InitConfigService,
    private platform: Platform
  ) { } 

  ngOnInit() {

    // eligiendo para la aplicaciòn si es cordova o Web
    if (this.plataformaDefault) {
      // caso que sea  cordova o web definida por el usuario

      if (this.plataformaDefault === 'cordova') {
        this.myV.nowCordova = true;
        console.log('>>> Definida Plataforma por el Desarrollador como CORDOVA');
      } else {
        this.myV.nowCordova = false;
        console.log('>>> Definida Plataforma por el Desarrollador como WEB');
      }


      ////////////////////////////////////////////////////
    } else {
      // caso seleccionado de modo automático

      if (this.platform.is('cordova')) {
        this.myV.nowCordova = true;
        console.log('>>> Plataforma Seleccioanda Automaticamente:: CORDOVA');
      } else {
        this.myV.nowCordova = false;
        console.log('>>> Plataforma Seleccioanda Automaticamente:: WEB');
      }

    }



    let url = new URL(window.location.href);

    // lectura de los parametros de la url root
    let mesa = url.searchParams.get(this.parametroUrlMesa);
    let tiendaUid = url.searchParams.get(this.parametroUrlTkwRestUid);

    if (this.myV.nowCordova) {
      console.log('>>> Iniciando sistema Cordova');
      this.continuarInicio();
    } else if (mesa && tiendaUid) {
      console.log('>>> Iniciando sistema con MESA en modo WEB');
      this.usingMesa(mesa, tiendaUid);
    } else {
      console.log('>>> Falta mesa o uid de tienda');
      this.router.navigateByUrl('/url-not-allowed')
    }
  }

  /**
   *  CASO cuando se tiene 'tkwrestboard' en la url base
   * @param mesa :string
   */
  usingMesa(mesa: string, tiendaUid: string) {

    // ESTA FUNCIÓN GUARDA para su uso en splash
    // indicar que es el local en => this.myV.intoLocal
    // la mesa  en                => this.myV.mesaCode
    // el uid   en                => this.myV.uid

    const timeInto = moment().unix();
    this.myV.intoLocal = timeInto;
    this.myV.mesaCode = mesa;
    this.myV.tiendaLocalUid = tiendaUid;

    // actualizo al nuevo uid para el inicio de la App
    this.myF.actualizarUid(tiendaUid);

    this.myS
      .set('appClienteIntoLocalTime', timeInto)
      .subscribe((x) =>
        console.log('Almacenado time inicio dentro del local -> ', x)
      );
    console.log('timeInto:', timeInto);
    console.warn(
      'Hemos Asignado Una mesa en nuestro local Inicialmente dispones de 3 horas asignado a esta mesa'
    );
    setTimeout(() => {
      this.continuarInicio();
    }, 1000);
  }

  /**
   *
   * @param kiosco CASO cuando se tiene 'kiosco' en la url base
   */
  usingKiosco(kiosco: string) {
    this.myV.kioscoCode = kiosco;
    this.continuarInicio();
  }

  continuarInicio() {
    setTimeout(() => {
      this.initConfig.initializeApp();
    }, 20);
  }
}
